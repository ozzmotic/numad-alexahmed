package edu.neu.madcourse.alexahmed.windwardTest;

public interface AudioClipListener
{
    public boolean heard(short[] audioData, int sampleRate);
}
