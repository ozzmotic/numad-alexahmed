package edu.neu.madcourse.alexahmed.bananagrams;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

import edu.neu.madcourse.alexahmed.R;

public class Tile {
    private Drawable d;
    private String letter;
    private Paint letterPaint;
    private int width;
    private int column;
    private int row;
    public boolean scored = false;

    // Constructor
    public Tile(Resources res, int x, int y, int w, String s, int txtsize) {
        // Get bitmap from resource file
        d = res.getDrawable(R.drawable.tile);
        letter = s;
        width = w;
        letterPaint = new Paint();
        letterPaint.setColor(Color.BLACK);
        letterPaint.setTextSize(txtsize);

        // Store coordinates
        setBounds(x, y);
    }

    public Tile() {
    }

    // Render bitmap at current location
    public void doDraw(Canvas canvas) {
        d.draw(canvas);
        canvas.drawText(getLetter(), getTileBounds().centerX(), getTileBounds().centerY(), letterPaint);
    }

    public Rect getTileBounds() {
        return d.getBounds();
    }
    public String getLetter() {
        return letter;
    }
    public int getColumn() { return column; }
    public int getRow() { return row; }
    public int getWidth() { return width; }

    public void setBounds(int x, int y)  {
        column = width * (Math.round(x / width));
        row = width * (Math.round(y / width));
        d.setBounds(column, row, column + width, row + width);
    }

    public void setLetter(String s) {
        letter = s;
    }

    public void setWidth(int s) {
        width = s;
    }

    public void setResources() {
        d = Board.getMyContext().getResources().getDrawable(R.drawable.tile);
    }

    public void setPaint() {
        letterPaint = new Paint();
        letterPaint.setColor(Color.BLACK);
        letterPaint.setTextSize(Board.pxToDp(15));
    }

    public void unscoreTile() {
        scored = false;
        letterPaint.setColor(Color.BLACK);
    }

    public void scoreTile() {
        scored = true;
        letterPaint.setColor(Color.GREEN);
    }

    public boolean isScored() {
        return scored;
    }

    public String serialize() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Tile.class, new TileDeserializer());
        gsonBuilder.registerTypeAdapter(Tile.class, new TileSerializer());
        Gson gson = gsonBuilder.create();

        return gson.toJson(this);
    }

    public static Tile create(String serializedData) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Tile.class, new TileDeserializer());
        gsonBuilder.registerTypeAdapter(Tile.class, new TileSerializer());
        Gson gson = gsonBuilder.create();

        return gson.fromJson(serializedData, Tile.class);
    }

    private static class TileSerializer implements JsonSerializer<Tile> {
        public JsonElement serialize(Tile t, Type type, JsonSerializationContext jsc) {
            JsonObject jo = new JsonObject();
            jo.addProperty("letter",t.getLetter());
            jo.addProperty("column",t.getColumn());
            jo.addProperty("row",t.getRow());
            jo.addProperty("scored",t.isScored());
            jo.addProperty("width",t.getWidth());
            return jo;
        }
    }

    private static class TileDeserializer implements JsonDeserializer<Tile> {
        public Tile deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
            JsonObject jo = je.getAsJsonObject();
            Tile t = new Tile();
            t.setResources();
            t.setPaint();
            t.setLetter(jo.getAsJsonPrimitive("letter").getAsString());
            t.setWidth(jo.getAsJsonPrimitive("width").getAsInt());
            t.setBounds(jo.getAsJsonPrimitive("column").getAsInt(),
                    jo.getAsJsonPrimitive("row").getAsInt());

            if (jo.getAsJsonPrimitive("scored").getAsBoolean()) {
                t.scoreTile();
            } else {
                t.unscoreTile();
            }

            return t;
        }
    }
}
