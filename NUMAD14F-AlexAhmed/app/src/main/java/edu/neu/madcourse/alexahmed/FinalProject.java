package edu.neu.madcourse.alexahmed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

public class FinalProject extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_app_desc);

        TextView txtView = (TextView)findViewById(R.id.windward_des_c);
        txtView.setMovementMethod(new ScrollingMovementMethod());
    }

    public void launchProject(View v) {
        Intent intent = new Intent("android.intent.action.START_WINDWARD");
        startActivity(intent);
    }
}
