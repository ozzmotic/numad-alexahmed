package edu.neu.madcourse.alexahmed;

import edu.neu.madcourse.alexahmed.bananagrams.Bananagrams;
import edu.neu.madcourse.alexahmed.bananagrams.Bananagrams_Multiplayer;
import edu.neu.madcourse.alexahmed.bananagrams.Music;
import edu.neu.madcourse.alexahmed.communication.Communication;
import edu.neu.madcourse.alexahmed.dictionary.WordDBHelper;
import edu.neu.madcourse.alexahmed.sudoku.Sudoku;
import edu.neu.madcourse.alexahmed.dictionary.Dictionary;
import edu.neu.madcourse.alexahmed.windwardTest.MainMenu;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {

    private static Context myContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myContext = getApplicationContext();
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    protected void onStop() {
        Music.stop(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        WordDBHelper.closeHard();
        super.onDestroy();
    }
    
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
    
    public static class AboutDialogFragment extends DialogFragment {
    	@Override
    	public Dialog onCreateDialog(Bundle savedInstanceState) {
    	
    		setRetainInstance(true);
    		String aboutme = "Alex Ahmed is a first-year student in the " +
    				"Personal Health Informatics PhD program at " +
    				"Northeastern University.\n\nAlex can be reached at " +
    				"ahmed.al@husky.neu.edu.\n\nFinal project team: Alex Ahmed and Xuan Li." +
                    "\n\nYour phone's IMEI is ";
    		
    		String imeistring = null;
    		TelephonyManager telephonyManager;
    		telephonyManager = getManager();

    		imeistring = telephonyManager.getDeviceId();
    		
    		String fullMessage = aboutme + imeistring + ".";
    		
    		AlertDialog.Builder myDialog = new AlertDialog.Builder(getActivity());
    		
    		myDialog.setTitle("About this app");
    		myDialog.setIcon(R.drawable.head);
    		myDialog.setMessage(fullMessage);
    		
    		myDialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
    		
    		return myDialog.create();
    	}
    }

    private static TelephonyManager getManager() {
        return (TelephonyManager)myContext.getSystemService(Context.TELEPHONY_SERVICE);
    }
    
    public void QuitApp(View v) {
        finish();
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
    
    public void CrashMe(){
    	//Crash due to no View specified in input
    }
    
    public void ShowAboutDialog(View v) {
    	DialogFragment aboutDialog = new AboutDialogFragment();
    	aboutDialog.show(getSupportFragmentManager(),"about");
    }
    
    public void StartSudoku(View v) {
    	Intent intent = new Intent(this, Sudoku.class);
    	startActivity(intent);
    }
    
    public void OpenDictionary(View v) {
        CharSequence text = "Loading dictionary...";
        int duration = Toast.LENGTH_SHORT;
        Toast.makeText(getApplicationContext(), text, duration).show();

    	Intent intent = new Intent(this, Dictionary.class);
    	startActivity(intent);
    }

    public void StartBananagrams(View v) {
        Intent intent = new Intent(this, Bananagrams.class);
        startActivity(intent);
    }

    public void Communication(View v) {
        Intent intent = new Intent(this, Communication.class);
        startActivity(intent);
    }

    public void StartMultiPlayerBananagrams(View v) {
        Intent intent = new Intent(this, Bananagrams_Multiplayer.class);
        startActivity(intent);
    }

    public void windwardTest(View v) {
        Intent intent = new Intent(this, MainMenu.class);
        startActivity(intent);
    }

    public void openFinalProjectDescription(View v) {
        Intent intent = new Intent(this, FinalProject.class);
        startActivity(intent);
    }
}
