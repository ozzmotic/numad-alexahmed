package edu.neu.madcourse.alexahmed.bananagrams;

import edu.neu.madcourse.alexahmed.dictionary.WordDBHelper;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import edu.neu.madcourse.alexahmed.R;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private Paint mPaint;
    private Paint dark;
    private Paint timerText;
    private static float mWidth;
    private static float mHeight;
    private ViewThread mThread;
    private static String TAG = "GameView";
    private boolean tileSelected = false;
    private Tile lastTouched;
    private Tile[][] grid;
    private int tilesInBank = 144;
    private int[] tileBank;
    private int tileSide;
    private int gridLength;
    private int boardEdgeRight;
    private int boardEdgeBottom;
    private int gridWidth;
    private int currentScore;
    private int opponentScore;
    private WordDBHelper db = null;
    private final long timeOut = 120 * 1000;
    private final long interval = 1000;
    private long elapsed;
    private int secondsLeft;
    private MyTimerTask timerTask;
    private Timer timer;
    private boolean isPaused;
    private boolean mMultiplayer;
    private Activity boardActivity;
    private static Context mContext;
    private String lastScored = "";

    public GameView (Context context, float width, float height, boolean multiplayer) {
        super(context);
        getHolder().addCallback(this);
        mWidth = width;
        mHeight = height;
        mContext = context;
        mMultiplayer = multiplayer;
        //Create as many rows/columns as needed
        gridLength = 12;
        gridWidth = 8;
        tileSide = (int)mHeight / (gridLength + 4);
        boardEdgeRight = gridWidth * (tileSide + 1);
        boardEdgeBottom = gridLength * (tileSide + 1);

        grid = new Tile[gridLength][gridWidth];

        //Configure colors
        mPaint = new Paint();
        mPaint.setColor(getResources().getColor(R.color.puzzle_background));
        dark = new Paint();
        dark.setColor(getResources().getColor(R.color.puzzle_dark));
        timerText = new Paint();
        timerText.setColor(Color.RED);
        timerText.setTextSize(pxToDp(12));

        //Preallocate and fill tiles array
        tileBank = new int[tilesInBank];
        //Populate tile bank
        for (int n=0; n < tileBank.length; n++) {
            tileBank[n] = n+1;
        }

        //Configure dictionary
        db = WordDBHelper.getDBadapterInstance(context);
        try {
            db.createDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        db.openDatabase();

        if (!mMultiplayer) {
            timerTask = new MyTimerTask();
            timer = new Timer();
        }
        mThread = new ViewThread(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!mThread.isAlive()) {
            mThread = new ViewThread(this);
            mThread.setRunning(true);
            mThread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Store new extents
        mWidth = width;
        mHeight = height;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //Stop thread
        if (mThread.isAlive()) {
            mThread.setRunning(false);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int touchX = (int)event.getX();
        int touchY = (int)event.getY();
        int[] gridLocation = getRowAndColumn(touchX,touchY);

        if (gridLocation[0] < 0 || gridLocation[0] > gridLength - 1) {
            return false;
        } else if (gridLocation[1] < 0 || gridLocation[1] > gridWidth - 1) {
            return false;
        }

        //TODO Touch and drag tiles
        //Check to see if touched a tile last time, move it to new location
        if (tileSelected && grid[gridLocation[0]][gridLocation[1]] == null) {
            int[] lastTouchedLocation = getRowAndColumn(lastTouched.getTileBounds().centerX(),lastTouched.getTileBounds().centerY());
            grid[lastTouchedLocation[0]][lastTouchedLocation[1]] = null;
            grid[gridLocation[0]][gridLocation[1]] = lastTouched;
            lastTouched.setBounds(touchX, touchY);
            tileSelected = false;
            checkTiles(lastTouchedLocation[0],lastTouchedLocation[1]);
            checkTiles(gridLocation[0],gridLocation[1]);
        } else {
            outerloop:
            for (Tile[] tileArray : grid) {
                for (Tile tile : tileArray ) {
                    if (tile != null) {
                        //Check if touch was inside a current tile
                        if (touchX >= tile.getTileBounds().left && touchX <= tile.getTileBounds().right) {
                            if (touchY >= tile.getTileBounds().top && touchY <= tile.getTileBounds().bottom) {
                                tileSelected = true;
                                lastTouched = tile;
                                break outerloop;
                            } else {
                                tileSelected = false;
                            }
                        } else {
                            tileSelected = false;
                        }
                    }
                }
            }
            synchronized (grid) {
                //Draw a tile if there are any left, and if there is no tile there currently
                if (!tileSelected && tilesInBank > 0) {
                    if (grid[gridLocation[0]][gridLocation[1]] == null) {
                        int drawnTile = drawTile();
                        grid[gridLocation[0]][gridLocation[1]] = new Tile(getResources(),
                                touchX, touchY, tileSide, numToString(drawnTile), pxToDp(15));
                        checkTiles(gridLocation[0], gridLocation[1]);
                    }
                }
            }
        }
        return super.onTouchEvent(event);
    }

    public class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            elapsed += interval;
            if (elapsed >= timeOut) {
                this.cancel();
                Log.i(TAG,"finished");

                boardActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((Board) Board.getMyContext()).endGame(currentScore);
                    }
                });
            }
            if (isPaused) {
                this.cancel();
                Log.i(TAG,"paused");
            }
        }
    }

    public int[] getRowAndColumn(int x, int y) {
        int[] coords = new int[2];
        int column = Math.round(x / tileSide);
        int row = Math.round(y / tileSide);
        coords[0] = row;
        coords[1] = column;
        return coords;
    }

    public void doDraw(Canvas canvas) {
        canvas.drawPaint(mPaint);

        //Draw horizontal lines
        for (int i = 0; i <= gridLength; i++) {
            canvas.drawLine(0, i * tileSide, boardEdgeRight, i * tileSide, dark);
        }
        //Draw vertical lines
        for (int i = 0; i <= gridWidth; i++) {
            canvas.drawLine(i * tileSide, 0, i * tileSide, boardEdgeBottom, dark);
        }

        canvas.drawText("Your score: " + currentScore, pxToDp(200), boardEdgeBottom + pxToDp(15), timerText);

        if (mMultiplayer) {
            canvas.drawText("Time left: " + secondsLeft, pxToDp(20), boardEdgeBottom + pxToDp(15), timerText);
            canvas.drawText("Opponent's score: " + opponentScore, pxToDp(200), boardEdgeBottom + pxToDp(30), timerText);

            if (secondsLeft < 31) {
                canvas.drawText("Time is running out!", pxToDp(20), boardEdgeBottom + pxToDp(30), timerText);
            }
        } else {
            canvas.drawText("Time left: " + ((timeOut - elapsed)/1000), pxToDp(20), boardEdgeBottom + pxToDp(15), timerText);

            if ((timeOut - elapsed)/1000 < 31) {
                canvas.drawText("Time is running out!", pxToDp(20), boardEdgeBottom + pxToDp(30), timerText);
            }
        }

        synchronized (grid) {
            for (Tile[] tileArray : grid) {
                for (Tile tile : tileArray) {
                    if (tile != null) {
                        tile.doDraw(canvas);
                    }
                }
            }
        }
    }

    private int drawTile() {
        int randomNum = randInt(0,tilesInBank-1);
        int pick = tileBank[randomNum];
        int currentLast = tileBank[tilesInBank-1];
        tileBank[tilesInBank-1] = tileBank[randomNum];
        tileBank[randomNum] = currentLast;
        tilesInBank--;
        currentScore--;

        if (mMultiplayer) {
            ((Bananagrams_Multiplayer) Bananagrams_Multiplayer.getMyContext()).setMyScore(currentScore);
        }
        return pick;
    }

    private String numToString(int num) {
        if (num < 19) { return "E"; }
        if (num < 32) { return "A"; }
        if (num < 44) { return "I"; }
        if (num < 56) { return "O"; }
        if (num < 64) { return "T"; }
        if (num < 73) { return "R"; }
        if (num < 81) { return "N"; }
        if (num < 87) { return "D"; }
        if (num < 93) { return "S"; }
        if (num < 99) { return "U"; }
        if (num < 104) { return "L"; }
        if (num < 108) { return "G"; }
        if (num < 111) { return "B"; }
        if (num < 114) { return "C"; }
        if (num < 117) { return "F"; }
        if (num < 120) { return "H"; }
        if (num < 123) { return "M"; }
        if (num < 126) { return "P"; }
        if (num < 129) { return "V"; }
        if (num < 132) { return "W"; }
        if (num < 135) { return "Y"; }
        if (num < 137) { return "J"; }
        if (num < 139) { return "K"; }
        if (num < 141) { return "Q"; }
        if (num < 143) { return "X"; }
        return "Z";
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    //Loop through tiles trying to find any word matches
    public boolean checkTiles(int x, int y) {
        boolean gotColMatch = false;
        boolean gotRowMatch = false;
        int wordStart = 0;
        int wordEnd = 0;

        String col = "";
        //Check column for words (constant Y)
        if (grid[x][y] != null) {
            if (x > 0 && x < gridLength - 1) { //not an edge tile
                if (grid[x-1][y] != null) { // check for tiles above
                    if (grid[x+1][y] != null) { //check for tiles below
                        //tiles above and below: go up in the grid, find start point of word
                        //then find endpoint
                        wordStart = searchUp(x,y);
                        wordEnd = searchDown(x,y);
                    } else { // only tiles above, so go up in the grid
                        wordStart = searchUp(x,y);
                        wordEnd = x; //the ending is the origin tile
                    }
                } else { //no tiles above
                    if (grid[x+1][y] != null) { //tiles below, but not above: go down in the grid
                        wordStart = x;
                        wordEnd = searchDown(x,y);
                    } else { // no tiles above or below: start and end is origin tile
                        wordStart = x;
                        wordEnd = x;
                    }
                }
            } else if (x == 0) { //top edge, only look down
                wordStart = x; // the beginning is the origin tile
                wordEnd = searchDown(x,y);
            } else if (x == gridLength - 1) { //bottom edge, only look up
                wordStart = searchUp(x,y);
                wordEnd = x; //the ending is the origin tile
            }
            col = findColumnWord(wordStart, wordEnd, y);
            gotColMatch = getMatches(col, wordStart, y, true, gotColMatch);
        } else { //checking a null tile (space)
            if (x > 0 && x < gridLength - 1) { //not an edge space
                if (grid[x-1][y] != null) { // check for tiles above
                    if (grid[x + 1][y] != null) { //check for tiles below
                        //Search above the space for a word, then below (two words possible)
                        wordStart = searchUp(x,y);
                        wordEnd = x - 1; //the ending is the space before origin tile
                        col = findColumnWord(wordStart, wordEnd, y);
                        gotColMatch = getMatches(col, wordStart, y, true, gotColMatch);

                        wordEnd = searchDown(x,y);
                        wordStart = x + 1;
                        col = findColumnWord(wordStart, wordEnd, y);
                        gotColMatch = getMatches(col, wordStart, y, true, gotColMatch);
                    }  else { // only tiles above, so go up in the grid
                        wordStart = searchUp(x,y);
                        wordEnd = x - 1; //the ending is the space before the origin tile
                    }
                } else { //no tiles above
                    if (grid[x+1][y] != null) { //tiles below, but not above: go down in the grid
                        wordStart = x + 1;
                        wordEnd = searchDown(x,y);
                    } else { //no tiles above or below, no words possible
                        wordStart = x;
                        wordEnd = x;
                    }
                }
            } else if (x == 0) { //top edge, only look down
                wordStart = x + 1; // the beginning is the origin tile + 1
                wordEnd = searchDown(x,y);
            } else if (x == gridLength - 1) { //bottom edge, only look up
                wordStart = searchUp(x,y);
                wordEnd = x - 1; //the ending is the origin tile
            }
            col = findColumnWord(wordStart, wordEnd, y);
            gotColMatch = getMatches(col, wordStart, y, true, gotColMatch);
        }

        //Check row for words (constant X)
        String row = "";
        if (grid[x][y] != null) {
            if (y > 0 && y < gridWidth - 1) { //not an edge tile
                if (grid[x][y-1] != null) { // check for tiles to the left
                    if (grid[x][y+1] != null) { //check for tiles to the right
                        //tiles to both left and right: find start point of word then find endpoint
                        wordStart = searchLeft(x,y);
                        wordEnd = searchRight(x,y);
                    } else { // only tiles to left, so go left in the grid
                        wordStart = searchLeft(x,y);
                        wordEnd = y; //the ending is the origin tile
                    }
                } else { //no tiles to left
                    if (grid[x][y+1] != null) { //tiles to right, but not to left: go right in the grid
                        wordStart = y;
                        wordEnd = searchRight(x,y);
                    } else { // no tiles to left or right: start and end is origin tile
                        wordStart = y;
                        wordEnd = y;
                    }
                }
            } else if (y == 0) { //left edge, only look right
                wordStart = y; // the beginning is the origin tile
                wordEnd = searchRight(x,y);
            } else if (y == gridWidth - 1) {
                wordStart = searchLeft(x,y);
                wordEnd = y; //the ending is the origin tile
            }
            row = findRowWord(wordStart, wordEnd, x);
            gotRowMatch = getMatches(row, wordStart, x, false, gotColMatch);
        } else { //checking a null tile (space)
            if (y > 0 && y < gridWidth - 1) { //not an edge space
                if (grid[x][y-1] != null) {
                    if (grid[x][y+1] != null) {
                        wordStart = searchLeft(x,y);
                        wordEnd = y - 1;
                        row = findRowWord(wordStart, wordEnd, x);
                        gotRowMatch = getMatches(row, wordStart, x, false, gotColMatch);

                        wordEnd = searchRight(x,y);
                        wordStart = y + 1;
                        row = findRowWord(wordStart, wordEnd, x);
                        gotRowMatch = getMatches(row, wordStart, x, false, gotColMatch);
                    }  else {
                        wordStart = searchLeft(x,y);
                        wordEnd = y - 1;
                    }
                } else { //no tiles above
                    if (grid[x][y+1] != null) {
                        wordStart = y + 1;
                        wordEnd = searchRight(x,y);
                    } else {
                        wordStart = y;
                        wordEnd = y;
                    }
                }
            } else if (y == 0) {
                wordStart = y + 1;
                wordEnd = searchRight(x,y);
            } else if (y == gridWidth - 1) {
                wordStart = searchLeft(x,y);
                wordEnd = y - 1;
            }
            row = findRowWord(wordStart, wordEnd, x);
            gotRowMatch = getMatches(row, wordStart, x, false, gotColMatch);
        }
        return gotRowMatch || gotColMatch;
    }

    //Query = the string to check, x and y is the location of the last tile in the string.
    //isColumnWord is true if the word was found in a column - this is so that later the program
    //can trace back the word to the beginning, and make all those tiles as "scored"
    private boolean getMatches(String query, int start, int colOrRow, boolean isColumnWord, boolean gotMatch) {
        int length = query.length();
        Cursor matches = db.getWordMatches(query);
        if (matches != null && matches.getCount() > 0) {
            parseMatches(matches, length, start, colOrRow, isColumnWord);
            return true;
        } else if (matches != null && matches.getCount() < 1) {
            if (isColumnWord) {
                for (int n = 0; n < length; n++) { //unscore ONLY if the tile hasn't just been scored
                    if (!gotMatch) {
                        grid[start+n][colOrRow].unscoreTile();
                        Log.i(TAG,"UNSCORED " + grid[start+n][colOrRow].getLetter());
                    }
                }
            } else {
                for (int n = 0; n < length; n++) {
                    if (!gotMatch) {
                        grid[colOrRow][start+n].unscoreTile(); //unscore ONLY if the tile hasn't just been scored
                        Log.i(TAG,"UNSCORED " + grid[colOrRow][start+n].getLetter());
                    }
                }
            }
        }
        return false;
    }

    private void parseMatches(Cursor c, int wordLength, int start, int colOrRow, boolean isColumnWord) {
        int index = c.getColumnIndex("content");
        c.moveToFirst();
        while(!c.isAfterLast()) {
            String data = c.getString(index);

            if (!data.equals(lastScored)) {
                Log.i(TAG,"Scored word: " + data);
                Music.playSoundEffect(mContext, R.raw.bell);
                currentScore = currentScore + 10;
                lastScored = data;
            } else {
                Log.i(TAG,"Tried to cheat!");
            }

            if (mMultiplayer) {
                ((Bananagrams_Multiplayer) Bananagrams_Multiplayer.getMyContext()).setMyScore(currentScore);
                Log.i(TAG,"setMyScore called");
            }

            //Go back through the tiles that were scored, and score them
            if (isColumnWord) {
                for (int n = 0; n < wordLength; n++) {
                    grid[start+n][colOrRow].scoreTile();
                    Log.i(TAG,"Scored " + grid[start+n][colOrRow].getLetter());
                }
            } else {
                for (int n = 0; n < wordLength; n++) {
                    grid[colOrRow][start+n].scoreTile();
                    Log.i(TAG,"Scored " + grid[colOrRow][start+n].getLetter());
                }
            }
            c.moveToNext();
        }
        c.close();
    }

    public String findColumnWord(int start, int end, int column) {
        String word = "";
        for (int n = start; n <= end; n++) {
            if (grid[n][column] == null) { return word; }
            word = word + grid[n][column].getLetter();
        }
        return word;
    }

    public String findRowWord(int start, int end, int row) {
        String word = "";
        for (int n = start; n <= end; n++) {
            if (grid[row][n] == null) { return word; }
            word = word + grid[row][n].getLetter();
        }
        return word;
    }

    //Grid searching methods
    private int searchUp(int x, int y) {
        int wordStart = 0;
        for (int n = 1; n < gridLength/2 + 1; n++) { //go up in grid until null or end
            if (x - n < 0) {
                wordStart = 0;
                break;
            }
            if (grid[x-n][y] == null) {
                wordStart = x - n + 1;
                break;
            }
        }
        return wordStart;
    }

    private int searchDown(int x, int y) {
        int wordEnd = 0;
        for (int n = 1; n < gridLength/2 + 1; n++) { // go down until null or end
            if (x + n == gridLength) {
                wordEnd = gridLength - 1;
                break;
            }
            if (grid[x+n][y] == null) {
                wordEnd = x + n - 1;
                break;
            }
        }
        return wordEnd;
    }

    private int searchLeft(int x, int y) {
        int wordStart = 0;
        for (int n = 1; n < gridWidth/2 + 1; n++) { //go left in grid until null or end
            if (y - n < 0) {
                wordStart = 0;
                break;
            }
            if (grid[x][y-n] == null) {
                wordStart = y - n + 1;
                break;
            }
        }
        return wordStart;
    }

    private int searchRight(int x, int y) {
        int wordEnd = 0;
        for (int n = 1; n < gridWidth/2 + 1; n++) { // go right until null or end
            if (y + n == gridWidth) {
                wordEnd = gridWidth - 1;
                break;
            }
            if (grid[x][y+n] == null) {
                wordEnd = y + n - 1;
                break;
            }
        }
        return wordEnd;
    }

    private int[] findFirstBlank() {
        int[] ret = new int[2];
        for (int x = 0; x < gridLength; x++) {
            for (int y = 0; y < gridWidth; y++) {
                if (grid[x][y] == null) {
                    ret[0] = x;
                    ret[1] = y;
                    return ret;
                }
            }
        }
        return null;
    }

    //Timer methods
    public void pauseTimer() {
        isPaused = true;
        Log.i(TAG,"Pausing timer, msec = " + elapsed);
    }

    public void resumeTimer() {
        isPaused = false;
        Log.i(TAG,"Resuming timer, msec = " + elapsed);
        timerTask = new MyTimerTask();
        timer.scheduleAtFixedRate(timerTask, interval, interval);
    }

    public void setBoardActivity(Activity activity){
        boardActivity = activity;
    }

    //Database Methods
    public void closeDB() {
        if (db.isDbOpen()) {
            db.close();
        }
    }

    public void reopenDB() {
        if (!db.isDbOpen()) {
            db.openDatabase();
        }
    }

    //Getters
    public int getCurrentScore() {
        return currentScore;
    }

    public long getTimeElapsed() {
        return elapsed;
    }

    public int getTilesInBank() {
        return tilesInBank;
    }

    public int[] getTileBank() {
        return tileBank;
    }

    //Setters
    public void setCurrentScore(int score) {
        currentScore = score;
    }

    public void setTimeElapsed(long e) {
        elapsed = e;
    }

    public void setTilesInBank(int t) {
        tilesInBank = t;
    }

    public void setTileBank(int[] t) {
        tileBank = t;
    }

    public void setOpponentScore(int s, boolean realTime) {
        Log.i(TAG,"opponent score set to " + s);
        opponentScore = s;
        if (realTime) {
            int[] blank = findFirstBlank();
            int drawnTile = drawTile();
            if (grid[blank[0]][blank[1]] == null) {
                grid[blank[0]][blank[1]] = new Tile(getResources(),
                        blank[0] * tileSide, blank[1] * tileSide, tileSide, numToString(drawnTile), pxToDp(15));
            }
        }
    }

    public void setSecondsLeft(int s) {
        secondsLeft = s;
    }

    public Set<String> serializeGrid() {
        Set<String> gridSet = new LinkedHashSet<String>();

        for (int x = 0; x < gridLength; x++) {
            for (int y = 0; y < gridWidth; y++) {
                if (grid[x][y] != null) {
                    gridSet.add(grid[x][y].serialize());
                }
            }
        }
        return gridSet;
    }

    public void restoreGrid(String[] set) {
        for (String s: set) {
            Tile tile = Tile.create(s);
            int[] loc = getRowAndColumn(tile.getTileBounds().centerX(),tile.getTileBounds().centerY());
            grid[loc[0]][loc[1]] = tile;
        }
    }

    public static int pxToDp(int px) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, mContext.getResources().getDisplayMetrics()));
    }
}
