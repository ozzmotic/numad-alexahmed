package edu.neu.madcourse.alexahmed.dictionary;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import edu.neu.madcourse.alexahmed.MainActivity;
import edu.neu.madcourse.alexahmed.R;

public class Dictionary extends Activity {

    private WordDBHelper db = null;
    private static String TAG = "Dictionary";
    private String[] matches;
    private int count;
    final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION,100);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dictionary_main);
        db = WordDBHelper.getDBadapterInstance(this);
        TextView textView = (TextView)findViewById(R.id.textView1);
        textView.setMovementMethod(new ScrollingMovementMethod());

        try {
            db.createDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        db.openDatabase();

        EditText editText = (EditText)findViewById(R.id.editText);
        editText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //do query
                String query = s.toString();
                if (s.length() > 2) {
                    Cursor c = db.getWordMatches(query);
                    if (c != null) { updateTextView(c); }
                }
            }
            public void afterTextChanged(Editable s) {}
            public void beforeTextChanged(CharSequence s, int start, int before, int count) {}
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        refresh();
    }

    @Override
    protected void onStop() {
        if (db.isDbOpen()) {
            db.close();
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        if (!db.isDbOpen()) {
            db.openDatabase();
        }
        super.onResume();
    }

    private void updateTextView(Cursor c) {
        int index = c.getColumnIndex("content");
        c.moveToFirst();
    	while(!c.isAfterLast()) {
    		String data = c.getString(index);

            if (!duplicateCheck(data)) {
                matches[count] = data;
                count++;
                tg.startTone(ToneGenerator.TONE_PROP_BEEP);
                TextView textView = (TextView)findViewById(R.id.textView1);
                textView.append(data + "\n");
            }
            c.moveToNext();
    	}
    	c.close();
    }

    private boolean duplicateCheck(String match) {
        if (count == 0) {return false;}
        for (String s: matches) {
            if (s != null) {
                if (s.equals(match)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void refresh() {
        matches = new String[50];
        count = 0;
    }

    //Button functions
    public void clearText(View v) {
        TextView textView = (TextView)findViewById(R.id.textView1);
        EditText editText = (EditText)findViewById(R.id.editText);
        textView.setText("");
        editText.setText("");
        refresh();
    }

    public void returnToMenu(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void openAcknowledgments(View v) {
        Intent intent = new Intent(this, Acknowledgments.class);
        startActivity(intent);
    }
}
