package edu.neu.madcourse.alexahmed.windwardTest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import edu.neu.madcourse.alexahmed.R;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private Paint backgroundPaint;
    private Paint dark;

    private float mWidth;
    private float mHeight;
    private ViewThread mThread;

    private static Context mContext;
    private final int OBJ_SHIP = 0;
    private final int OBJ_ISLAND = 1;

    private WorldObject[][] grid;
    private WorldObject temp;

    private int gridLength;
    private int gridWidth;
    private int tileSide;
    private int shipRow;
    private int shipCol;
    private boolean onIsland = false;

    private int mapSize;
    private Rect currentWindow;
    private int boardEdgeRight;
    private int boardEdgeBottom;

    public GameView (Context context, float width, float height) {
        super(context);
        getHolder().addCallback(this);

        mWidth = width;
        mHeight = height;
        mContext = context;

        mapSize = 1000;
        gridLength = 14;
        gridWidth = 14;
        tileSide = (int)mHeight / (gridLength + 4);

        boardEdgeRight = gridWidth * tileSide;
        boardEdgeBottom = gridLength * tileSide;

        //Initialize map grid
        grid = new WorldObject[mapSize][mapSize];

        //Paints
        backgroundPaint = new Paint();
        backgroundPaint.setColor(getResources().getColor(R.color.water));

        dark = new Paint();
        dark.setColor(getResources().getColor(R.color.black));

        //Place ship at starting location
        shipRow = 998;
        shipCol = 9;
        grid[shipRow][shipCol] = new WorldObject(tileSide,OBJ_SHIP);

        //Place an island
        grid[8][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[7][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[6][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[5][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[4][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[3][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[2][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[1][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[8][15] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[20][9] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[5][9] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[998][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[996][7] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[988][6] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[986][6] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[970][9] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[969][9] = new WorldObject(tileSide,OBJ_ISLAND);
        grid[966][9] = new WorldObject(tileSide,OBJ_ISLAND);

        updateWindow();
        setAllBoundsInWindow();
        mThread = new ViewThread(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!mThread.isAlive()) {
            mThread = new ViewThread(this);
            mThread.setRunning(true);
            mThread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //Stop thread
        if (mThread.isAlive()) {
            mThread.setRunning(false);
        }
    }

    public void moveShip() {
        if (shipRow == 0) { return; }
        WorldObject ship = grid[shipRow][shipCol];

        if (onIsland) {
            grid[shipRow][shipCol] = temp;
            onIsland = false;
        } else {
            grid[shipRow][shipCol] = null;
        }

        if (grid[shipRow-1][shipCol] != null) {
            if (grid[shipRow-1][shipCol].getType() == OBJ_ISLAND) {
                onIsland = true;
                temp = grid[shipRow-1][shipCol];
            } else {
                onIsland = false;
                temp = null;
            }
        }

        grid[shipRow-1][shipCol] = ship;
        shipRow--;
        updateWindow();
        setAllBoundsInWindow();
    }

    public void setAllBoundsInWindow() {
        synchronized (grid) {
            for (int x = 0; x < gridWidth; x++) {
                for (int y = 0; y < gridLength; y++) {
                    if (grid[currentWindow.top + x][currentWindow.left + y] != null) {
                        WorldObject obj = grid[currentWindow.top + x][currentWindow.left + y];
                        obj.setBounds(x * tileSide, y * tileSide);
                    }
                }
            }
        }
    }

    public int[] getRowAndColumn(int x, int y) {
        int[] coords = new int[2];
        coords[0] = Math.round(y / tileSide);
        coords[1] = Math.round(x / tileSide);
        return coords;
    }

    public void updateWindow() {
        int left = Math.round(shipCol - gridWidth/2);
        int bottom = Math.round(shipRow + 2);
        int top = bottom - (gridLength + 1);
        int right = Math.round(shipCol + gridWidth/2);

        if (left < 0) { left = 0; }
        if (right >= mapSize) { right = mapSize - 1; }
        if (top < 0) { top = 0; }
        if (bottom >= mapSize) { bottom = mapSize - 1; }

        currentWindow = new Rect(left, top, right, bottom);
    }

    public void doDraw(Canvas canvas) {
        canvas.drawPaint(backgroundPaint);

        /* For testing only
        //Draw horizontal lines
        for (int i = 0; i <= gridLength; i++) {
            canvas.drawLine(0, i * tileSide, boardEdgeRight, i * tileSide, dark);
        }
        //Draw vertical lines
        for (int i = 0; i <= gridWidth; i++) {
            canvas.drawLine(i * tileSide, 0, i * tileSide, boardEdgeBottom, dark);
        }
        */
        //Draw every object in the visible window
        synchronized (grid) {
            for (int x = 0; x < gridWidth; x++) {
                for (int y = 0; y < gridLength; y++) {
                    if (grid[currentWindow.top + x][currentWindow.left + y] != null) {
                        grid[currentWindow.top + x][currentWindow.left + y].doDraw(canvas);
                    }
                }
            }
        }
    }
}
