package edu.neu.madcourse.alexahmed.bananagrams;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import edu.neu.madcourse.alexahmed.MainActivity;
import edu.neu.madcourse.alexahmed.R;

public class PausedScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bananagrams_paused);
    }

    public void resumeGame (View v) {
        Intent intent = new Intent(this, Board.class);
        intent.putExtra("NEW",false);
        startActivity(intent);
    }

    public void quitGame (View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
