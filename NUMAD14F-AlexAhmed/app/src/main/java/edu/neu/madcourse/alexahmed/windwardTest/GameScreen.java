package edu.neu.madcourse.alexahmed.windwardTest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import edu.neu.madcourse.alexahmed.R;

public class GameScreen extends FragmentActivity implements View.OnClickListener {

    private static GameView gameView;
    public static Context context;
    private static String TAG = "GameScreen";

    private BreathListener breathListener;
    private BreathDetectTask breathDetectorTask;
    private boolean hasMic = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Microphone
        if (AudioManager.hasMicrophone(this)) {
            hasMic = true;

            breathListener = new BreathListener(2, 250, 1500, 200, 500, this);
            //breathListener.setDevice(0);

            breathDetectorTask = new BreathDetectTask(this);
            breathDetectorTask.execute(breathListener);
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        context = getApplicationContext();

        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = getDisplaySize(display);
        int mWidth = size.x;
        int mHeight = size.y;

        FrameLayout game = new FrameLayout(this);
        gameView = new GameView(this, mWidth, mHeight);

        LinearLayout gameWidgets = new LinearLayout(this);

        Button quitButton = new Button(this);
        Button moveButton = new Button(this);

        LinearLayout.LayoutParams quitButtonParams = new LinearLayout.LayoutParams(pxToDp(140),
                pxToDp(50));
        quitButtonParams.setMargins(5, mHeight - pxToDp(75), 5, 5);
        quitButton.setLayoutParams(quitButtonParams);
        quitButton.setText("Quit");
        quitButton.setTextSize(12);
        quitButton.setId(R.id.game_quit);
        quitButton.setOnClickListener(this);

        LinearLayout.LayoutParams moveButtonParams = new LinearLayout.LayoutParams(pxToDp(140),
                pxToDp(50));
        moveButtonParams.setMargins(5, mHeight - pxToDp(75), 5, 5);
        moveButton.setLayoutParams(quitButtonParams);
        moveButton.setText("Move!");
        moveButton.setTextSize(12);
        moveButton.setId(R.id.game_move);
        moveButton.setOnClickListener(this);

        gameWidgets.addView(moveButton);
        gameWidgets.addView(quitButton);

        game.addView(gameView);
        game.addView(gameWidgets);

        setContentView(game);

        Toast.makeText(this, "Take a deep breath and exhale",
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.game_quit:
                finish();
                Intent intent = new Intent(this, MainMenu.class);
                startActivity(intent);
                break;
            case R.id.game_move:
                gameView.moveShip();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (hasMic) {
            stopMic();
            breathDetectorTask = new BreathDetectTask(this);
            breathDetectorTask.execute(breathListener);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (hasMic) {
            stopMic();
        }
    }

    public void moveShip() {
        gameView.moveShip();
    }

    private static Point getDisplaySize(final Display display) {
        final Point point = new Point();
        if (Build.VERSION.SDK_INT > 12) {
            display.getSize(point);
        } else { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        return point;
    }

    public static int pxToDp(int px) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, getMyContext().getResources().getDisplayMetrics()));
    }

    public static Context getMyContext() {
        return context;
    }

    //Microphone methods
    private void stopMic() {
        shutDownTaskIfNecessary(breathDetectorTask);
    }

    private void shutDownTaskIfNecessary(final AsyncTask task) {
        if (task != null && !task.isCancelled()) {
            if (task.getStatus().equals(AsyncTask.Status.RUNNING)
                    || task.getStatus().equals(AsyncTask.Status.PENDING)) {
                task.cancel(true);
            } else {
                Log.i("Task", "Task not running");
            }
        }
    }

    public int getVolFromPref() {
        final SharedPreferences prefs = getPreferences(this.context);
        int volumeThreshold = prefs.getInt("volThreshold", -1);
        //Log.i("pref vol", volumeThreshold + " ");
        return volumeThreshold;
    }

    public int getFreqFromPref() {
        final SharedPreferences prefs = getPreferences(this.context);
        int freqThreshold = prefs.getInt("freqThreshold", -1);
        //Log.i("pref freq", freqThreshold + " ");
        return freqThreshold;
    }

    public int getRangeFromPref() {
        final SharedPreferences prefs = getPreferences(this.context);
        int rangeThreshold = prefs.getInt("rangeThreshold", -1);
        //Log.i("pref freq", rangeThreshold + " ");
        return rangeThreshold;
    }

    public void storeCalibrationData(int v, int f, int r) {
        final SharedPreferences prefs = getPreferences(this.context);
        //int appVersion = getAppVersion(this.context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("volThreshold", v);
        editor.putInt("freqThreshold", f);
        editor.putInt("rangeThreshold", r);
        editor.commit();

        Log.i("storeData", v + " " + f + " " + r);
    }

    // unneeded?
    /*
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }*/

    private SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(GameScreen.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
}