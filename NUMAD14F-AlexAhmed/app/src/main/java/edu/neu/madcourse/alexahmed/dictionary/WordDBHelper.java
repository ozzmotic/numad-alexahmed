package edu.neu.madcourse.alexahmed.dictionary;

import android.content.Context;
import android.database.sqlite.*;
import android.database.SQLException;
import android.util.Log;
import android.database.Cursor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.File;

public class WordDBHelper extends SQLiteOpenHelper {

    private static String TAG = "WordDBHelper";
    private static String DB_NAME = "worddb";
    private static String DB_PATH = "";
    private static SQLiteDatabase wordDB;
    private static Context mContext;
    private static WordDBHelper dbConnection;

    public WordDBHelper(Context context) {
        super(context,DB_NAME,null,1);
        mContext = context;
        Log.i(TAG,"Constructor called");
        if(android.os.Build.VERSION.SDK_INT >= 17){
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        } else {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
    }

    public static synchronized WordDBHelper getDBadapterInstance(Context context) {
        mContext = context;
        if (dbConnection == null) {
            dbConnection = new WordDBHelper(context);
        } return dbConnection;
    }

    public void createDatabase() throws IOException {} {
        Log.i(TAG,"createDatabase called");
        boolean dbExist = checkDatabase();

        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDatabase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDatabase() {
        File dbfile = new File(DB_PATH + DB_NAME);
        return dbfile.exists();
    }

    public void copyDatabase() throws IOException {
        Log.i(TAG,"copyDatabase called");
        if(android.os.Build.VERSION.SDK_INT >= 17){
            DB_PATH = mContext.getApplicationInfo().dataDir + "/databases/";
        } else {
            DB_PATH = "/data/data/" + mContext.getPackageName() + "/databases/";
        }
        InputStream myInput = mContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();

        Log.i(TAG,"Database copy complete");
    }

    public void openDatabase() throws SQLException {
        String myPath = DB_PATH + DB_NAME;
        wordDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.i(TAG,"onCreate called");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public static void closeHard() {
        if (wordDB != null && wordDB.isOpen()) {
            wordDB.close();
        }
    }

    public boolean isDbOpen() {
        return wordDB.isOpen();
    }

    public Cursor getWordMatches(String query) {
        String[] selectionArgs = new String[] {query};
        return wordDB.rawQuery("SELECT * FROM WORD_DB WHERE content MATCH ?", selectionArgs);
    }
}
