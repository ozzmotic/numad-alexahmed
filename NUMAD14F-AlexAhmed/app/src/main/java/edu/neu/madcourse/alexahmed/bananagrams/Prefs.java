package edu.neu.madcourse.alexahmed.bananagrams;

import edu.neu.madcourse.alexahmed.R;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Prefs extends PreferenceActivity {
    // Option names and default values
    private static final String OPT_BG_MUSIC = "music";
    private static final boolean OPT_BG_MUSIC_DEF = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.bgrams_settings);
    }

    /** Get the current value of the music option */
    public static boolean getMusic(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(OPT_BG_MUSIC, OPT_BG_MUSIC_DEF);
    }
}
