package edu.neu.madcourse.alexahmed.windwardTest;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import edu.neu.madcourse.alexahmed.R;

import java.lang.reflect.Type;

public class WorldObject {

    private Drawable d;
    private int type;
    private int column;
    private int row;
    private int width;

    public WorldObject(int w, int type) {
        setType(type);

        width = w;
    }

    public WorldObject() {
    }

    //Render bitmap at current location
    public void doDraw(Canvas canvas) {
        d.draw(canvas);
    }

    //Setters
    public void setBounds(int x, int y) {
        row = x;
        column = y;
        d.setBounds(column, row, column + width, row + width);
    }

    public void setType(int t) {
        type = t;

        switch (t) {
            case -1:
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.test);
                break;
            case 0:
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.ship);
                break;
            case 1:
                d = GameScreen.getMyContext().getResources().getDrawable(R.drawable.island);
                break;
        }
    }

    //Getters
    public int getType() {
        return type;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    //Serialization
    public String serialize() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(WorldObject.class, new WorldObjectDeserializer());
        gsonBuilder.registerTypeAdapter(WorldObject.class, new WorldObjectSerializer());
        Gson gson = gsonBuilder.create();

        return gson.toJson(this);
    }

    public static WorldObject create(String serializedData) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(WorldObject.class, new WorldObjectDeserializer());
        gsonBuilder.registerTypeAdapter(WorldObject.class, new WorldObjectSerializer());
        Gson gson = gsonBuilder.create();

        return gson.fromJson(serializedData, WorldObject.class);
    }

    private static class WorldObjectSerializer implements JsonSerializer<WorldObject> {
        public JsonElement serialize(WorldObject t, Type type, JsonSerializationContext jsc) {
            JsonObject jo = new JsonObject();
            jo.addProperty("type", t.getType());
            jo.addProperty("column", t.getColumn());
            jo.addProperty("row", t.getRow());
            return jo;
        }
    }

    private static class WorldObjectDeserializer implements JsonDeserializer<WorldObject> {
        public WorldObject deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
            JsonObject jo = je.getAsJsonObject();
            WorldObject t = new WorldObject();
            t.setType(jo.getAsJsonPrimitive("type").getAsInt());
            t.setBounds(jo.getAsJsonPrimitive("column").getAsInt(),
                    jo.getAsJsonPrimitive("row").getAsInt());

            return t;
        }
    }
}
