package edu.neu.madcourse.alexahmed.windwardTest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import edu.neu.madcourse.alexahmed.MainActivity;
import edu.neu.madcourse.alexahmed.R;

public class MainMenu extends Activity {



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_menu);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public void startGame(View v) {

        // for testing
        // do calibration again when we restart the level
        // BreathListener.doCalibration();

        Intent intent = new Intent(this, GameScreen.class);
        startActivity(intent);

    }

    public void quitGame(View v) {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
