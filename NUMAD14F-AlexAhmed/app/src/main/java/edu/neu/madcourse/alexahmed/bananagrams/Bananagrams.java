package edu.neu.madcourse.alexahmed.bananagrams;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import edu.neu.madcourse.alexahmed.MainActivity;
import edu.neu.madcourse.alexahmed.R;


public class Bananagrams extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bananagrams_main);
    }

    @Override
    protected void onStop() {
        Music.stop(this);
        super.onStop();
    }

    public void newGame(View v) {
        Intent intent = new Intent(this, Board.class);
        intent.putExtra("NEW",true);
        startActivity(intent);
    }

    public void exitGame(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void openAcknowledgments(View v) {
        Intent intent = new Intent(this, Acknowledgments.class);
        startActivity(intent);
    }

    public void continueGame(View v) {
        Intent intent = new Intent(this, Board.class);
        intent.putExtra("NEW",false);
        startActivity(intent);
    }

    public void openInstructions(View v) {
        Intent intent = new Intent(this, Instructions.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bg_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bgrams_settings:
                startActivity(new Intent(this, Prefs.class));
                return true;
        }
        return false;
    }
}
