package edu.neu.madcourse.alexahmed.windwardTest;

import android.content.Context;
import android.util.Log;

import java.util.LinkedList;

public class BreathListener implements AudioClipListener {
    private static final String TAG = "BreathListener";

    private LinkedList<Integer> frequencyHistory;

    public void setRangeThreshold(int rangeThreshold) {
        this.rangeThreshold = rangeThreshold;
    }

    public void setVolThreshold(int volThreshold) {
        this.volThreshold = volThreshold;
    }

    public void setFreqThreshold(int freqThreshold) {
        this.freqThreshold = freqThreshold;
    }

    private int rangeThreshold = 0;
    private int volThreshold = 0;
    private int freqThreshold = 0;

    public static final int DEFAULT_SILENCE_THRESHOLD = 2000;

    private static final boolean DEBUG = false;

    private long prevTime = 0;
    public static int i_freq = 0;
    public static int i_volume = 0;
    public static int i_freqRange = 0;
    private int breathTime = 0;
    private long prevBreathTime = 0;
    private int breathTimeThreshold = 200;
    private float interval = 500;
    private long prevIntervalTime = 0;

    public static int breathInc = 0;

    private MovingAvg movingAvgFreq, movingAvgVol;
    private int speedUp = 0;
    private GameScreen gameScreen;
    private int range = 0;

    // start calibration
    // global doCalibration flag
    public static boolean doCalibration = false;
    public static boolean startCalibration = false;
    public static boolean testStartCalibration = false;
    public static float sensitivity = 0.0f;

    private boolean doneProcessingCalibrationData = true;

    // calibration stage data to compare with preset threshold
    private int startCalibrationVolThreshold = 1000;
    private int volumePreCalibration = 0;
    private int c_rangeThreshold = 0;
    private int c_volThreshold = 0;
    private int c_freqThreshold = 0;
    private LinkedList<Integer> calibrationVol;
    private LinkedList<Integer> calibrationFreq;
    private LinkedList<Integer> calibrationRange;

    private Context classContext;

    // set from pref
    private boolean setFromPref = false;

    private long prepCalibrationTime;

    public void setDevice(int device) {
        // rise phone
        // 2, 50, 500, 200, 500
        // nexus 7
        // 2, 250, 1500, 200, 500

        if (device == 0) {
            this.freqThreshold = 500;
            this.rangeThreshold = 50;
            this.volThreshold = 3000;
        }

        if (device == 1) {
            this.freqThreshold = 1500;
            this.rangeThreshold = 250;
            this.volThreshold = 500;
        }

        if (device == 2) {
            this.freqThreshold = 500;
            this.rangeThreshold = 50;
            this.volThreshold = 1000;
        }

    }

    public BreathListener(int historySize, int rangeThreshold,
                          int freqThreshold, int breathTime, int volThreshold, GameScreen game) {

        classContext = GameScreen.context;
        gameScreen = game;

        calibrationVol = new LinkedList<Integer>();
        calibrationFreq = new LinkedList<Integer>();
        calibrationRange = new LinkedList<Integer>();

        calibrationRange.clear();

        frequencyHistory = new LinkedList<Integer>();
        // pre-fill so modification is easy
        for (int i = 0; i < historySize; i++) {
            frequencyHistory.add(Integer.MAX_VALUE);
        }

        this.freqThreshold = freqThreshold;
        this.rangeThreshold = rangeThreshold;
        this.volThreshold = volThreshold;
        this.breathTimeThreshold = breathTime;

        Log.d("initial threshold values", this.volThreshold + " "
                + this.freqThreshold + " " + this.rangeThreshold);

        prevTime = System.currentTimeMillis();
        prevBreathTime = System.currentTimeMillis();
        prevIntervalTime = System.currentTimeMillis();

        // good k values 8, 20, 6, 4
        movingAvgFreq = new MovingAvg(4);
        movingAvgVol = new MovingAvg(4);

        prepCalibrationTime = -1;
    }

    @Override
    public boolean heard(short[] audioData, int sampleRate) {
        int frequency = AudioUtil.ZeroCrossing(sampleRate, audioData);
        int volume = (int) AudioUtil.rootMeanSquared(audioData);

        movingAvgFreq.pushValue(frequency);
        frequency = (int) movingAvgFreq.getValue();

        movingAvgVol.pushValue(volume);
        volume = (int) movingAvgVol.getValue();

        // Log.i("first freq", frequency + "");

        frequencyHistory.addFirst(frequency);
        // since history is always full, just remove the last
        frequencyHistory.removeLast();
        range = calculateRange();

        // Log.i("new range", range + " ");

        if (DEBUG) {
            Log.d(TAG, "range: " + range + " freq " + frequency + " loud: "
                    + volume + " history " + frequencyHistory.size());
        }

        if (!setFromPref) {
            if (gameScreen.getVolFromPref() != -1) {
                volThreshold = gameScreen.getVolFromPref();
            }
            if (gameScreen.getFreqFromPref() != -1) {
                freqThreshold = gameScreen.getFreqFromPref();
            }
            if (gameScreen.getVolFromPref() != -1) {
                rangeThreshold = gameScreen.getRangeFromPref();
            }
            Log.d("pref threshold values", this.volThreshold + " "
                    + this.freqThreshold + " " + this.rangeThreshold);
            setFromPref = true;
        }

        prepareValuesForCalibration();
        if (System.currentTimeMillis() - prevTime > 150) {
            i_freq = frequency;
            i_volume = volume;
            i_freqRange = range;
            recordCalibrationData();
            prevTime = System.currentTimeMillis();
        }
        processCalibrationData();

        boolean heard = false;
        if (i_volume > volThreshold && i_freqRange < rangeThreshold
                && i_freq < freqThreshold) {

            breathTime = (int) (System.currentTimeMillis() - prevBreathTime);

            Log.d("breathTime", breathTime + "");
            if (breathTime > breathTimeThreshold) {

                speedUp += 1;

                if (interval > 80.f) {
                    interval -= (20.f + speedUp);
                }

                if (System.currentTimeMillis() - prevIntervalTime > interval) {
                    breathInc = breathInc + 1;
                    gameScreen.moveShip();

                    // Log.d(TAG, " " + breathInc);
                    heard = true;

                    prevIntervalTime = System.currentTimeMillis();
                }

            }

        } else {
            breathTime = 0;
            prevBreathTime = System.currentTimeMillis();
            speedUp = 0;

            if (interval < 500) {
                interval += 5.f;
                if (System.currentTimeMillis() - prevIntervalTime > interval) {
                    breathInc = breathInc + 1;
                    gameScreen.moveShip();
                    // Log.d(TAG, " " + breathInc);
                    prevIntervalTime = System.currentTimeMillis();
                }
            }
        }

        return heard;
    }

    private int calculateRange() {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (Integer val : frequencyHistory) {
            if (val >= max) {
                max = val;
            }

            if (val < min) {
                min = val;
            }
        }

		/*
		 * if (DEBUG) { StringBuilder sb = new StringBuilder(); for (Integer val
		 * : frequencyHistory) { sb.append(val).append(" "); } Log.d(TAG,
		 * sb.toString() + " [" + (max - min) + "]"); }
		 */

        return Math.abs(max - min);
    }

    // do calibration again
    public static void doCalibration() {
        doCalibration = false;
        testStartCalibration = false;
    }

    private void prepareValuesForCalibration() {
        if (prepCalibrationTime == -1) {
            prepCalibrationTime = System.currentTimeMillis();
        }

        if (!doCalibration && !testStartCalibration) {
            if (System.currentTimeMillis() - prepCalibrationTime > 2000 && range < 10000) {
                BreathListener.testStartCalibration = true;
                Log.i("start calibration", "toast");
            }
        }
    }

    private void recordCalibrationData() {
        if (!doCalibration) {

			/*
			if (!testStartCalibration && !startCalibration) {
				// set before calibration
				volumePreCalibration = i_volume;
				// Log.i("vol pre calibration", "volume pre calibration");
			}*/

            // start Calibration is allowed to be true only if current vol is at
            // lease % greater than
            // previous vol
            if (testStartCalibration && !startCalibration) {
                if (i_volume > startCalibrationVolThreshold /*volumePreCalibration * 1.5f*/ ) {
                    startCalibration = true;
                    Log.i("vol pre calibration", "start calibration");
                }
            }

            if (startCalibration) {

                Log.i("vol pre calibration", "adding calibration values");
                // save a list of calibration breathing values for vol, freq,
                // freq range
                doneProcessingCalibrationData = false;
                calibrationVol.add(i_volume);
                calibrationFreq.add(i_freq);
                calibrationRange.add(i_freqRange);

                if (calibrationVol.size() > 10) {
                    startCalibration = false;
                }
            }
        }
    }

    private void processCalibrationData() {
        if (!doCalibration) {
            if (!startCalibration) {
                if (!doneProcessingCalibrationData) {
                    // calculate the average of each parameter

                    // VOLUME
                    if (!calibrationVol.isEmpty()) {
                        for (int i = 0; i < calibrationVol.size(); i++) {
                            c_volThreshold += calibrationVol.get(i);
                        }
                        c_volThreshold = c_volThreshold / calibrationVol.size();
                        calibrationVol.clear();
                    }

                    // FREQUENCY
                    if (!calibrationFreq.isEmpty()) {
                        for (int i = 0; i < calibrationFreq.size(); i++) {
                            c_freqThreshold += calibrationFreq.get(i);
                        }
                        c_freqThreshold = c_freqThreshold
                                / calibrationFreq.size();
                        calibrationFreq.clear();
                    }

                    // RANGE
                    if (!calibrationRange.isEmpty()) {
                        for (int i = 0; i < calibrationRange.size(); i++) {
                            c_rangeThreshold += calibrationRange.get(i);
                            Log.i("range debug", calibrationRange.get(i)
                                    + " size " + calibrationRange.size());
                        }
                        c_rangeThreshold = Math.abs(c_rangeThreshold
                                / calibrationRange.size()); // /
                        // calibrationRange.size();
                        calibrationRange.clear();
                    }

                    Log.d("Calibrated threshold values", c_volThreshold + " "
                            + c_freqThreshold + " " + c_rangeThreshold);

                    // sensitivity -0.5 -> 0.5
                    // apply sensitivity adjustment
                    // vol
                    // * (1-sensitivity)

                    volThreshold = (int) (c_volThreshold * (1 - sensitivity));

                    // freq and rang
                    // * (1+sensitivty)
                    freqThreshold = (int) (c_freqThreshold * (1 + sensitivity));
                    rangeThreshold = (int) (c_rangeThreshold * 1 + sensitivity);

                    // storeCalibrationData(classContext);
                    gameScreen.storeCalibrationData(volThreshold,
                            freqThreshold, rangeThreshold);
                    int tvol = gameScreen.getVolFromPref();

                    testStartCalibration = false;
                    doneProcessingCalibrationData = true;
                    doCalibration = true;
                }
            }
        }
    }

}