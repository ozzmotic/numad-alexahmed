package edu.neu.madcourse.alexahmed.bananagrams;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import java.util.Iterator;
import java.util.Set;
import edu.neu.madcourse.alexahmed.R;

public class Board extends FragmentActivity implements View.OnClickListener {

    private static GameView gameView;
    private static Context context;
    private static Activity myActivity;
    private static int endScore;
    private static int bestScore;
    private String PREFS_NAME = "GAME_DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        boolean newGame = getIntent().getBooleanExtra("NEW",true);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        context = this;
        myActivity = this;

        WindowManager wm = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = getDisplaySize(display);
        int mWidth = size.x;
        int mHeight = size.y;

        FrameLayout game = new FrameLayout(this);
        gameView = new GameView(this, mWidth, mHeight, false);
        gameView.setBoardActivity(myActivity);

        SharedPreferences preferencesReader = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        if (!newGame) {
            gameView.setCurrentScore(preferencesReader.getInt("SCORE",0));
            gameView.setTileBank(getTileBankFromPrefs());
            gameView.setTilesInBank(preferencesReader.getInt("TILES_LEFT",144));
            gameView.setTimeElapsed(preferencesReader.getLong("ELAPSED_TIME",0));
            gameView.restoreGrid(getGridFromPrefs());
        }

        bestScore = preferencesReader.getInt("BEST_SCORE",0);

        LinearLayout gameWidgets = new LinearLayout(this);

        Button pauseButton = new Button(this);
        Button quitButton = new Button(this);

        LinearLayout.LayoutParams pauseButtonParams = new LinearLayout.LayoutParams(pxToDp(140), pxToDp(50));
        pauseButtonParams.setMargins(5,mHeight-pxToDp(75),5,5);
        pauseButton.setLayoutParams(pauseButtonParams);
        pauseButton.setText("Pause");
        pauseButton.setTextSize(12);
        pauseButton.setId(R.id.bgrams_pause);
        pauseButton.setOnClickListener(this);

        LinearLayout.LayoutParams quitButtonParams = new LinearLayout.LayoutParams(pxToDp(140), pxToDp(50));
        quitButtonParams.setMargins(0,mHeight-pxToDp(75),5,5);
        quitButton.setLayoutParams(quitButtonParams);
        quitButton.setText("Quit");
        quitButton.setTextSize(12);
        quitButton.setId(R.id.bgrams_quit);
        quitButton.setOnClickListener(this);

        gameWidgets.addView(pauseButton);
        gameWidgets.addView(quitButton);

        game.addView(gameView);
        game.addView(gameWidgets);

        setContentView(game);
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.reopenDB();
        gameView.resumeTimer();
        Music.play(this, R.raw.bgrams_music);
    }

    @Override
    protected void onStop() {
        gameView.closeDB();
        gameView.setBoardActivity(myActivity);
        Music.stop(this);
        super.onStop();
    }

    @Override
    protected void onPause() {
        gameView.pauseTimer();
        gameView.closeDB();
        Music.stop(this);

        SharedPreferences preferencesReader = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencesReader.edit();

        Set<String> gridSet = gameView.serializeGrid();
        Iterator iter = gridSet.iterator();
        int count = 0;
        while (iter.hasNext()) {
            editor.putString("TILE" + count,iter.next().toString());
            count++;
        }

        editor.putLong("ELAPSED_TIME",gameView.getTimeElapsed());
        editor.putInt("TILES_LEFT",gameView.getTilesInBank());
        editor.putInt("SCORE",gameView.getCurrentScore());

        count = 0;
        for (int i: gameView.getTileBank()) {
            editor.putInt("BANK_VALUE" + count++, i);
            count++;
        }
        editor.apply();

        super.onPause();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bgrams_pause:
                gameView.pauseTimer();
                Music.stop(this);
                Intent intent = new Intent(this, PausedScreen.class);
                startActivity(intent);
                break;

            case R.id.bgrams_quit:
                gameView.pauseTimer();
                Music.stop(this);
                quitToMenu();
                break;
        }
    }

    public static class endGameDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            setRetainInstance(true);
            AlertDialog.Builder myDialog = new AlertDialog.Builder(getActivity());
            myDialog.setTitle("Game over!");
            myDialog.setMessage("Your score was " + endScore + "! Your current high score is " + bestScore + "!");
            myDialog.setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    quitToMenu();
                }
            });
            myDialog.setPositiveButton("New Game", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    restartGame();
                }
            });
            return myDialog.create();
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            quitToMenu();
        }
    }

    public static void quitToMenu() {
        Intent intent = new Intent(getMyContext(), Bananagrams.class);
        myActivity.finish();
        myActivity.startActivity(intent);
    }

    public static void restartGame() {
        Intent intent = new Intent(getMyContext(), Board.class);
        myActivity.finish();
        myActivity.startActivity(intent);
    }

    public void endGame(int score) {
        endScore = score;
        if (endScore > bestScore) {
            bestScore = endScore;
            SharedPreferences preferencesReader = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferencesReader.edit();
            editor.putInt("BEST_SCORE", bestScore);
            editor.commit();
        }
        DialogFragment endGameDialogFragment = new endGameDialog();
        endGameDialogFragment.show(getSupportFragmentManager(), "end");
    }

    public static void unPauseGame() {
        gameView.resumeTimer();
    }

    public static Context getMyContext() {
        return context;
    }

    public static int pxToDp(int px) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, getMyContext().getResources().getDisplayMetrics()));
    }

    private static Point getDisplaySize(final Display display) {
        final Point point = new Point();
        if (Build.VERSION.SDK_INT > 12) {
            display.getSize(point);
        } else { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        return point;
    }

    public int[] getTileBankFromPrefs() {
        int[] ret = new int[144];
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        for (int i = 0; i < 144; i++) {
            ret[i] = prefs.getInt("BANK_VALUE" + i, i);
        }
        return ret;
    }

    public String[] getGridFromPrefs() {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String[] ret = new String[144-gameView.getTilesInBank()];
        for (int i = 0; i < 144-gameView.getTilesInBank(); i++) {
            ret[i] = prefs.getString("TILE" + i, null);
        }
        return ret;
    }
}
