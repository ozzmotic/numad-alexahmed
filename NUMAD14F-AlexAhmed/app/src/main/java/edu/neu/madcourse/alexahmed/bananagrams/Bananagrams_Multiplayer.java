package edu.neu.madcourse.alexahmed.bananagrams;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantResult;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMultiplayer;
import com.google.android.gms.plus.Plus;
import com.google.example.games.basegameutils.BaseGameUtils;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.neu.madcourse.alexahmed.MainActivity;
import edu.neu.madcourse.alexahmed.R;

public class Bananagrams_Multiplayer extends Activity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener,
        RoomUpdateListener, RoomStatusUpdateListener, RealTimeMessageReceivedListener,
        OnInvitationReceivedListener {

    final static String TAG = "Bgrams_MP";

    //NETWORK VARIABLES
    private static final int RC_SIGN_IN = 9001;
    final static int RC_SELECT_PLAYERS = 10002;
    final static int RC_INVITATION_INBOX = 9002;
    final static int RC_REQUEST_LEADERBOARD = 9003;
    final static int RC_WAITING_ROOM = 10001;
    final static int RC_RECOVER_PLAY_SERVICES = 10003;
    final static int RC_LOOK_AT_MATCHES = 10011;
    final static int RC_SELECT_PLAYERS_TURNBASED = 10012;

    private GoogleApiClient myApiClient;
    private OnInvitationReceivedListener mListener;
    private String mIncomingInvitationId;

    private boolean mResolvingConnectionFailure = false;
    private boolean mSignInClicked = false;
    private boolean mAutoStartSignInFlow = true;

    String mRoomId = null;
    boolean mMultiplayer = false;

    ArrayList<Participant> mParticipants = null;

    public String mMyId = null;

    //GAME VARIABLES
    int mSecondsLeft = -1; // how long until the game ends (seconds)
    final static int GAME_DURATION = 120; // game duration, seconds.
    static int mScore = 0; // user's current score
    static int oppScore = 0; //opponent's current score
    Map<String, Integer> mParticipantScore = new HashMap<String, Integer>();
    // Participants who sent us their final score.
    Set<String> mFinishedParticipants = new HashSet<String>();

    //GAME SCREEN
    private static GameView gameView;
    private static Context context;
    private static Activity myActivity;
    private static int mWidth;
    private static int mHeight;

    //Notifications
    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;

    //Turn based
    private TurnBasedMatch mTurnBasedMatch;
    public BananagramsTurn mTurnData;
    private int turnCount;
    private boolean isDoingTurn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bgrams_mp_main);

        //Multiplayer
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        myApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .build();

        mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_action_gamepad)
                .setContentTitle("New invitation to play Bananagrams!")
                .setContentText("Tap to view");

        //Game board
        context = this;
        myActivity = this;

        WindowManager wm = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = getDisplaySize(display);
        mWidth = size.x;
        mHeight = size.y;

        gameView = new GameView(this, mWidth, mHeight, true);
        gameView.setBoardActivity(myActivity);
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.reopenDB();
        checkPlayServices();
    }

    @Override
    protected void onPause() {
        gameView.closeDB();
        Music.stop(this);
        super.onPause();
    }

    public void onStop() {
        Log.d(TAG, "**** got onStop");

        //Close the database
        gameView.closeDB();

        // if we're in a room, leave it.
        leaveRoom();

        // if we're in a turn based match, leave it
        if (mTurnBasedMatch != null && isDoingTurn) {
            Games.TurnBasedMultiplayer.leaveMatch(myApiClient, mTurnBasedMatch.getMatchId())
                    .setResultCallback(new ResultCallback<TurnBasedMultiplayer.LeaveMatchResult>() {
                        @Override
                        public void onResult(TurnBasedMultiplayer.LeaveMatchResult result) {
                            processResult(result);
                        }
                    });
        }

        // stop trying to keep the screen on
        stopKeepingScreenOn();

        if (myApiClient == null || !myApiClient.isConnected()){
            switchToScreen(R.id.screen_sign_in);
        }
        else {
            switchToScreen(R.id.screen_wait);
        }
        super.onStop();
    }

    @Override
    public void onStart() {
        switchToScreen(R.id.screen_wait);
        if (myApiClient != null && myApiClient.isConnected()) {
            Log.w(TAG, "GameHelper: client was already connected on onStart()");
        } else {
            Log.d(TAG,"Connecting client.");
            myApiClient.connect();
        }

        switchToMainScreen();
        super.onStart();
    }

    // Handle back key to make sure we cleanly leave a game if we are in the middle of one
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e) {
        if (keyCode == KeyEvent.KEYCODE_BACK && mCurScreen == R.id.screen_game) {
            leaveRoom();
            return true;
        }
        return super.onKeyDown(keyCode, e);
    }

    private boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                showErrorDialog(status);
            } else {
                Toast.makeText(this, "This device is not supported.",
                        Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    void showErrorDialog(int code) {
        GooglePlayServicesUtil.getErrorDialog(code, this,
                RC_RECOVER_PLAY_SERVICES).show();
    }

    //Buttons
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.sign_in_button) {
            // user wants to sign in
            Log.d(TAG, "Sign-in button clicked");
            mSignInClicked = true;
            myApiClient.connect();
        }
        else if (view.getId() == R.id.sign_out_button) {
            // sign out.
            Log.d(TAG, "Sign-out button clicked");
            mSignInClicked = false;
            Games.signOut(myApiClient);
            myApiClient.disconnect();
            switchToScreen(R.id.screen_sign_in);
        }
    }

    public void openAcknowledgments(View v) {
        Intent intent = new Intent(this, Acknowledgments_MP.class);
        startActivity(intent);
    }

    public void openInstructions(View v) {
        Intent intent = new Intent(this, Instructions_MP.class);
        startActivity(intent);
    }

    //Game methods
    public void invitePlayers_tb(View v) {
        Intent intent = Games.TurnBasedMultiplayer.getSelectOpponentsIntent(myApiClient,
                1, 1, true);
        startActivityForResult(intent, RC_SELECT_PLAYERS_TURNBASED);
    }

    public void backToMainMenu(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void invitePlayers(View v) {
        Intent intent = Games.RealTimeMultiplayer.getSelectOpponentsIntent(myApiClient,1,1);
        switchToScreen(R.id.screen_wait);
        startActivityForResult(intent, RC_SELECT_PLAYERS);
    }

    public void seeInvitations(View v) {
        Intent intent = Games.Invitations.getInvitationInboxIntent(myApiClient);
        switchToScreen(R.id.screen_wait);
        startActivityForResult(intent, RC_INVITATION_INBOX);
    }

    public void acceptPopupInvitation(View v) {
        acceptInviteToRoom(mIncomingInvitationId);
        mIncomingInvitationId = null;
    }

    //Real time match start
    private void startGame(boolean multiplayer) {
        mMultiplayer = multiplayer;
        switchToGameView(true);

        // run the gameTick() method every second to update the game.
        final Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSecondsLeft <= 0)
                    return;
                gameTick();
                h.postDelayed(this, 1000);
            }
        }, 1000);
    }

    void gameTick() {
        if (mSecondsLeft > 0) {
            --mSecondsLeft;
            gameView.setSecondsLeft(mSecondsLeft);
        }

        if (mSecondsLeft <= 0) {
            // finish game
            broadcastScore(true);
            mMultiplayer = false;
            Games.Leaderboards.submitScore(myApiClient, getString(R.string.bgrams_leaderboardID), mScore);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Game over! Your score was " + mScore + ". Your opponent's score" +
                    " was " + oppScore + ".");

            alertDialogBuilder
                    .setCancelable(false)
                    .setNegativeButton("Back to Menu",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    switchToMenuView();
                                    switchToMainScreen();
                                }
                            })
                    .setPositiveButton("View Leaderboard",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    Games.Leaderboards.submitScore(myApiClient, getString(R.string.bgrams_leaderboardID), mScore);
                                    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(myApiClient,
                                            getString(R.string.bgrams_leaderboardID)), RC_REQUEST_LEADERBOARD);
                                }
                            });

            alertDialogBuilder.show();
        }
    }

    //Turn based match start
    private void startGame(boolean multiplayer, TurnBasedMatch match) {
        resetGameVars();
        mMultiplayer = multiplayer;
        switchToGameView(true);
        mTurnBasedMatch = match;

        if (mTurnData == null) {
            mTurnData = new BananagramsTurn(); //create a turn object if this is turn 1
        }

        oppScore = mTurnData.score; //Take current score value as opponent's score
        gameView.setOpponentScore(oppScore, false);
        turnCount = mTurnData.turnCounter; //get turn counter

        mTurnData = new BananagramsTurn();
        mTurnData.turnCounter = turnCount; //re-populate the turn number

        mMyId = Games.Players.getCurrentPlayerId(myApiClient);

        // run the gameTick() method every second to update the game.
        final Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSecondsLeft <= 0)
                    return;
                gameTickTurnBased();
                h.postDelayed(this, 1000);
            }
        }, 1000);
    }

    void gameTickTurnBased() {
        if (mSecondsLeft > 0) {
            --mSecondsLeft;
            gameView.setSecondsLeft(mSecondsLeft);
        }

        if (mSecondsLeft <= 0) {
            // finish game
            mMultiplayer = false;
            isDoingTurn = false;
            String nextParticipantId = getNextParticipantId();
            // Create the next turn
            turnCount++;
            mTurnData.turnCounter = turnCount;
            mTurnData.score = mScore; //replace the opponent's score with your score to save it

            String matchId = mTurnBasedMatch.getMatchId();
            String myParticipantId = mTurnBasedMatch.getParticipantId(mMyId);

            //if this is the second player's turn, finish the match after their turn
            if (mTurnData.turnCounter > 1) {
                ParticipantResult result1;
                ParticipantResult result2;
                if (mScore > oppScore) {
                    result1 = new ParticipantResult(myParticipantId, ParticipantResult.MATCH_RESULT_WIN, 1);
                    result2 = new ParticipantResult(nextParticipantId, ParticipantResult.MATCH_RESULT_LOSS, 2);
                } else if (mScore < oppScore) {
                    result1 = new ParticipantResult(myParticipantId, ParticipantResult.MATCH_RESULT_LOSS, 2);
                    result2 = new ParticipantResult(nextParticipantId, ParticipantResult.MATCH_RESULT_WIN, 1);
                } else {
                    result1 = new ParticipantResult(myParticipantId, ParticipantResult.MATCH_RESULT_TIE, 2);
                    result2 = new ParticipantResult(nextParticipantId, ParticipantResult.MATCH_RESULT_TIE, 1);
                }
                List<ParticipantResult> resultList = new ArrayList<ParticipantResult>();
                resultList.add(result1);
                resultList.add(result2);

                Log.i(TAG,"Finishing match");
                Games.TurnBasedMultiplayer.finishMatch(myApiClient, mTurnBasedMatch.getMatchId(),
                        mTurnData.persist(), resultList);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Game over! Your score was " + mScore + ". Your opponent's score" +
                        " was " + oppScore + ".");

                alertDialogBuilder
                        .setCancelable(false)
                        .setNeutralButton("Back to Menu",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        Log.i(TAG, "Game over, switching back to menu");
                                        mTurnData = null;
                                        switchToMenuView();
                                        switchToMainScreen();
                                    }
                                });

                alertDialogBuilder.show();
            } else {
                Games.TurnBasedMultiplayer.takeTurn(myApiClient, matchId,
                        mTurnData.persist(), nextParticipantId);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Your score was " + mScore + ". Now it's your opponent's turn!");

                alertDialogBuilder
                        .setCancelable(false)
                        .setNeutralButton("Back to Menu",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        Log.i(TAG, "Game over, switching back to menu");
                                        mTurnData = null;
                                        switchToMenuView();
                                        switchToMainScreen();
                                    }
                                });

                alertDialogBuilder.show();
            }
        }
    }

    public String getNextParticipantId() {

        String myParticipantId = mTurnBasedMatch.getParticipantId(mMyId);
        ArrayList<String> participantIds = mTurnBasedMatch.getParticipantIds();

        int desiredIndex = -1;

        for (int i = 0; i < participantIds.size(); i++) {
            if (participantIds.get(i).equals(myParticipantId)) {
                desiredIndex = i + 1;
            }
        }

        if (desiredIndex < participantIds.size()) {
            return participantIds.get(desiredIndex);
        }

        if (mTurnBasedMatch.getAvailableAutoMatchSlots() <= 0) {
            // You've run out of automatch slots, so we start over.
            return participantIds.get(0);
        } else {
            // You have not yet fully automatched, so null will find a new
            // person to play against.
            return null;
        }
    }

    public void updateMatch(TurnBasedMatch match) {
        mTurnBasedMatch = match;

        int status = match.getStatus();
        int turnStatus = match.getTurnStatus();

        switch (status) {
            case TurnBasedMatch.MATCH_STATUS_CANCELED:
                showWarning("Canceled!", "This game was canceled!");
                return;
            case TurnBasedMatch.MATCH_STATUS_EXPIRED:
                showWarning("Expired!", "This game is expired.  So sad!");
                return;
            case TurnBasedMatch.MATCH_STATUS_AUTO_MATCHING:
                showWarning("Waiting for auto-match...",
                        "We're still waiting for an automatch partner.");
                return;
            case TurnBasedMatch.MATCH_STATUS_COMPLETE:
                if (turnStatus == TurnBasedMatch.MATCH_TURN_STATUS_COMPLETE) {
                    showWarning(
                            "Complete!",
                            "This game is over; someone finished it, and so did you!  There is nothing to be done.");
                    break;
                }

                // Note that in this state, you must still call "Finish" yourself,
                // so we allow this to continue.
                showWarning("Complete!",
                        "This game is over; someone finished it!  You can only finish it now.");
        }

        // OK, it's active. Check on turn status.
        switch (turnStatus) {
            case TurnBasedMatch.MATCH_TURN_STATUS_MY_TURN:
                mTurnData = BananagramsTurn.unpersist(mTurnBasedMatch.getData());
                startGame(true, mTurnBasedMatch);
                return;
            case TurnBasedMatch.MATCH_TURN_STATUS_THEIR_TURN:
                // Should return results.
                showWarning("Alas...", "It's not your turn.");
                break;
            case TurnBasedMatch.MATCH_TURN_STATUS_INVITED:
                showWarning("Good inititative!",
                        "Still waiting for invitations.\n\nBe patient!");
        }

        mTurnData = null;

    }

    private void processResult(TurnBasedMultiplayer.CancelMatchResult result) {
        if (!checkStatusCode(null, result.getStatus().getStatusCode())) {
            return;
        }

        isDoingTurn = false;

        showWarning("Match",
                "This match is canceled.  All other players will have their game ended.");
    }

    private void processResult(TurnBasedMultiplayer.InitiateMatchResult result) {
        TurnBasedMatch match = result.getMatch();

        if (!checkStatusCode(match, result.getStatus().getStatusCode())) {
            return;
        }

        if (match.getData() != null) {
            // This is a game that has already started, so I'll just start
            updateMatch(match);
            return;
        }

        startGame(true, match);
    }


    private void processResult(TurnBasedMultiplayer.LeaveMatchResult result) {
        TurnBasedMatch match = result.getMatch();
        if (!checkStatusCode(match, result.getStatus().getStatusCode())) {
            return;
        }
        isDoingTurn = (match.getTurnStatus() == TurnBasedMatch.MATCH_TURN_STATUS_MY_TURN);
        showWarning("Left", "You've left this match.");
    }


    public void processResult(TurnBasedMultiplayer.UpdateMatchResult result) {
        TurnBasedMatch match = result.getMatch();
        if (!checkStatusCode(match, result.getStatus().getStatusCode())) {
            return;
        }
        if (match.canRematch()) {
            askForRematch();
        }

        isDoingTurn = (match.getTurnStatus() == TurnBasedMatch.MATCH_TURN_STATUS_MY_TURN);

        if (isDoingTurn) {
            updateMatch(match);
        }
    }

    // Rematch dialog
    public void askForRematch() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setMessage("Do you want a rematch?");

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Sure, rematch!",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                rematch();
                            }
                        })
                .setNegativeButton("No.",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                switchToMenuView();
                                switchToMainScreen();
                            }
                        });

        alertDialogBuilder.show();
    }

    // If you choose to rematch, then call it and wait for a response.
    public void rematch() {
        Games.TurnBasedMultiplayer.rematch(myApiClient, mTurnBasedMatch.getMatchId()).setResultCallback(
                new ResultCallback<TurnBasedMultiplayer.InitiateMatchResult>() {
                    @Override
                    public void onResult(TurnBasedMultiplayer.InitiateMatchResult result) {
                        processResult(result);
                    }
                });
        mTurnBasedMatch = null;
        isDoingTurn = false;
    }

    //Activity handling
    @Override
    public void onActivityResult(int request, int response, Intent data) {
        if (request == RC_SELECT_PLAYERS) {
            if (response != Activity.RESULT_OK) {
                //user canceled
                switchToMainScreen();
                return;
            }

            //get the invitee list
            final ArrayList<String> invitees = data.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);
            Log.d(TAG, "Invitee count: " + invitees.size());

            Bundle autoMatchCriteria = null;
            int minAutoMatchPlayers =
                    data.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
            int maxAutoMatchPlayers =
                    data.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);

            if (minAutoMatchPlayers > 0) {
                autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                        minAutoMatchPlayers, maxAutoMatchPlayers, 0);
            } else {
                autoMatchCriteria = null;
            }

            Log.d(TAG, "Creating room...");
            RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
            rtmConfigBuilder.addPlayersToInvite(invitees);
            rtmConfigBuilder.setMessageReceivedListener(this);
            rtmConfigBuilder.setRoomStatusUpdateListener(this);
            if (autoMatchCriteria != null) {
                rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
            }
            switchToScreen(R.id.screen_wait);
            keepScreenOn();
            resetGameVars();
            Games.RealTimeMultiplayer.create(myApiClient, rtmConfigBuilder.build());
            Log.d(TAG, "Room created, waiting for it to be ready...");
        }

        if (request == RC_SELECT_PLAYERS_TURNBASED) {
            if (response != Activity.RESULT_OK) {
                // user canceled
                return;
            }

            // Get the invitee list.
            final ArrayList<String> invitees =
                    data.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);

            // Get auto-match criteria.
            Bundle autoMatchCriteria = null;
            int minAutoMatchPlayers = data.getIntExtra(
                    Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
            int maxAutoMatchPlayers = data.getIntExtra(
                    Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);
            if (minAutoMatchPlayers > 0) {
                autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                        minAutoMatchPlayers, maxAutoMatchPlayers, 0);
            } else {
                autoMatchCriteria = null;
            }

            TurnBasedMatchConfig tbmc = TurnBasedMatchConfig.builder()
                    .addInvitedPlayers(invitees)
                    .setAutoMatchCriteria(autoMatchCriteria)
                    .build();

            // Start the match
            ResultCallback<TurnBasedMultiplayer.InitiateMatchResult> cb = new ResultCallback<TurnBasedMultiplayer.InitiateMatchResult>() {
                @Override
                public void onResult(TurnBasedMultiplayer.InitiateMatchResult result) {
                    processResult(result);
                }
            };
            resetGameVars();
            Games.TurnBasedMultiplayer.createMatch(myApiClient, tbmc).setResultCallback(cb);
        }

        if (request == RC_LOOK_AT_MATCHES) {
            // Returning from the 'Select Match' dialog

            if (response != Activity.RESULT_OK) {
                // user canceled
                return;
            }

            TurnBasedMatch match = data
                    .getParcelableExtra(Multiplayer.EXTRA_TURN_BASED_MATCH);

            if (match != null) {
                resetGameVars();
                updateMatch(match);
            }

            Log.d(TAG, "Match = " + match);
        }

        if (request == RC_WAITING_ROOM) {
            if (response == Activity.RESULT_OK) {
                //start game
                Log.d(TAG, "Starting game (waiting room returned OK).");
                startGame(true);
            }
            else if (response == Activity.RESULT_CANCELED) {
                //leave the room:
                leaveRoom();
            }
            else if (response == GamesActivityResultCodes.RESULT_LEFT_ROOM) {
                // player wants to leave the room
                leaveRoom();
            }
        }

        if (request == RC_INVITATION_INBOX) {
            handleInvitationInboxResult(response, data);
        }

        if (request == RC_RECOVER_PLAY_SERVICES) {
            if (response == RESULT_CANCELED) {
                Toast.makeText(this, "Google Play Services must be installed.",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        if (request == RC_SIGN_IN) {
            Log.d(TAG, "onActivityResult with requestCode == RC_SIGN_IN, responseCode="
                    + response + ", intent=" + data);
            mSignInClicked = false;
            mResolvingConnectionFailure = false;
            if (response == RESULT_OK) {
                myApiClient.connect();
            } else {
                BaseGameUtils.showActivityResultError(this, request, response,
                        R.string.signin_failure, R.string.signin_other_error);
            }
        }
        super.onActivityResult(request, response, data);
    }

    //Callbacks
    public void onConnected(Bundle connectionHint) {
        Log.d(TAG, "onConnected() called. Sign in successful!");
        Log.d(TAG, "Sign-in succeeded.");

        // register listener so we are notified if we receive an invitation to play
        // while we are in the game
        Games.Invitations.registerInvitationListener(myApiClient, this);
        boolean respToInvite = false;
        if (getIntent().getBooleanExtra("RT_invitation",false)) {
            Intent intent = Games.Invitations.getInvitationInboxIntent(myApiClient);
            switchToScreen(R.id.screen_wait);
            respToInvite = true;
            startActivityForResult(intent, RC_INVITATION_INBOX);
        } else if (getIntent().getBooleanExtra("TB_invitation",false)) {
            Intent intent = Games.TurnBasedMultiplayer.getInboxIntent(myApiClient);
            switchToScreen(R.id.screen_wait);
            respToInvite = true;
            startActivityForResult(intent, RC_LOOK_AT_MATCHES);
        }

        if (connectionHint != null && !respToInvite) {
            Log.d(TAG, "onConnected: connection hint provided. Checking for invite.");
            Invitation inv = connectionHint
                    .getParcelable(Multiplayer.EXTRA_INVITATION);
            mTurnBasedMatch = connectionHint.getParcelable(Multiplayer.EXTRA_TURN_BASED_MATCH);

            if (inv != null && inv.getInvitationId() != null) {
                // retrieve and cache the invitation ID
                Log.d(TAG,"onConnected: connection hint has a room invite!");
                acceptInviteToRoom(inv.getInvitationId());
                return;
            }

            if (mTurnBasedMatch != null) {
                if (myApiClient == null || !myApiClient.isConnected()) {
                    Log.d(TAG, "Warning: accessing TurnBasedMatch when not connected");
                }

                updateMatch(mTurnBasedMatch);
                return;
            }

        }
        if (mTurnBasedMatch == null) {
            switchToMainScreen();
        }
    }

    // Displays your inbox. You will get back onActivityResult where
    // you will need to figure out what you clicked on.
    public void onCheckGamesClicked(View view) {
        Intent intent = Games.TurnBasedMultiplayer.getInboxIntent(myApiClient);
        startActivityForResult(intent, RC_LOOK_AT_MATCHES);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended() called.");
        //myApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed() called, result: " + connectionResult);

        if (mResolvingConnectionFailure) {
            Log.d(TAG, "onConnectionFailed() ignoring connection failure; already resolving.");
            return;
        }

        if (mSignInClicked || mAutoStartSignInFlow) {
            mAutoStartSignInFlow = false;
            mSignInClicked = false;
            mResolvingConnectionFailure = BaseGameUtils.resolveConnectionFailure(this, myApiClient,
                    connectionResult, RC_SIGN_IN, getString(R.string.signin_other_error));
        }

        switchToScreen(R.id.screen_sign_in);
    }

    @Override
    public void onRoomCreated(int statusCode, Room room) {
        Log.d(TAG, "onRoomCreated(" + statusCode + ", " + room + ")");
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            Log.e(TAG, "*** Error: onRoomCreated, status " + statusCode);
            showGameError();
            return;
        }

        // show the waiting room UI
        showWaitingRoom(room);
    }

    @Override
    public void onJoinedRoom(int statusCode, Room room) {
        Log.d(TAG, "onJoinedRoom(" + statusCode + ", " + room + ")");
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            Log.e(TAG, "*** Error: onRoomConnected, status " + statusCode);
            showGameError();
            return;
        }

        // show the waiting room UI
        showWaitingRoom(room);
    }

    @Override
    public void onRoomConnected(int statusCode, Room room) {
        Log.d(TAG, "onRoomConnected(" + statusCode + ", " + room + ")");
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            Log.e(TAG, "*** Error: onRoomConnected, status " + statusCode);
            showGameError();
            return;
        }
        updateRoom(room);
    }

    // Called when we get an invitation to play a game. We react by showing that to the user.
    @Override
    public void onInvitationReceived(Invitation invitation) {
        // We got an invitation to play a game! So, store it in
        // mIncomingInvitationId
        // and show the popup on the screen.
        Intent resultIntent = new Intent(this, Bananagrams_Multiplayer.class);
        if (invitation.getInvitationType() == Invitation.INVITATION_TYPE_REAL_TIME) {
            resultIntent.putExtra("RT_invitation", true);
        } else {
            resultIntent.putExtra("TB_invitation", true);
        }

        PendingIntent resultPendingIntent;
        if (android.os.Build.VERSION.SDK_INT > 15) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addParentStack(Bananagrams_Multiplayer.class);
            stackBuilder.addNextIntent(resultIntent);
            resultPendingIntent = stackBuilder.getPendingIntent(0,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            resultPendingIntent = PendingIntent.getActivity(this,
                    0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        mBuilder.setContentIntent(resultPendingIntent);
        mNotificationManager.notify(0, mBuilder.build());

        mIncomingInvitationId = invitation.getInvitationId();
        ((TextView) findViewById(R.id.incoming_invitation_text)).setText(
                invitation.getInviter().getDisplayName() + " is inviting you!");
        switchToScreen(mCurScreen); // This will show the invitation popup
    }

    @Override
    public void onInvitationRemoved(String invitationId) {
        if (mIncomingInvitationId.equals(invitationId)) {
            mIncomingInvitationId = null;
            switchToScreen(mCurScreen); // This will hide the invitation popup
        }
    }

    @Override
    public void onPeersConnected(Room room, List<String> peers) {
        updateRoom(room);
    }

    @Override
    public void onPeersDisconnected(Room room, List<String> peers) {
        updateRoom(room);
    }

    @Override
    public void onPeerLeft(Room room, List<String> peers) {
        updateRoom(room);
    }

    @Override
    public void onPeerDeclined(Room room, List<String> peers) {
        updateRoom(room);
    }

    @Override
    public void onLeftRoom(int statusCode, String roomId) {
        Log.d(TAG, "onLeftRoom, code " + statusCode);
        switchToMenuView();
        switchToMainScreen();
    }

    @Override
    public void onPeerJoined(Room room, List<String> strings) {
        updateRoom(room);
    }

    @Override
    public void onDisconnectedFromRoom(Room room) {
        mRoomId = null;
        showGameError();
    }

    void showGameError() {
        BaseGameUtils.makeSimpleDialog(this, "Problem with the game");
        switchToMenuView();
        switchToMainScreen();
    }

    void updateRoom(Room room) {
        if (room != null) {
            mParticipants = room.getParticipants();
        }
        if (mParticipants != null) {
            //updatePeerScoresDisplay();
        }
    }

    //Messaging
    @Override
    public void onRealTimeMessageReceived(RealTimeMessage rtm) {
        // get real-time message
        byte[] buf = rtm.getMessageData();

        if (buf[0] == 'L') {
            showWarning("Quitter","Your opponent left the game!");
            Games.RealTimeMultiplayer.leave(myApiClient, this, mRoomId);
            mRoomId = null;
            return;
        }

        int len = buf.length;
        byte[] num = new byte[len-1];
        System.arraycopy(buf, 1, num, 0, len-1);
        String sender = rtm.getSenderParticipantId();
        //Log.d(TAG, "Message received: " + (char) buf[0] + "/" + (int) buf[1]);

        if (buf[0] == 'F' || buf[0] == 'U') {
            // score update.
            int existingScore = mParticipantScore.containsKey(sender) ?
                    mParticipantScore.get(sender) : 0;
            int thisScore = bytesToInt(num);
            if (thisScore > existingScore) {
                // this check is necessary because packets may arrive out of
                // order, so we
                // should only ever consider the highest score we received, as
                // we know in our
                // game there is no way to lose points. If there was a way to
                // lose points,
                // we'd have to add a "serial number" to the packet.
                mParticipantScore.put(sender, thisScore);

                // update the scores on the screen
                gameView.setOpponentScore(thisScore, true);
                oppScore = thisScore;
            }

            // if it's a final score, mark this participant as having finished
            // the game
            if ((char) buf[0] == 'F') {
                mFinishedParticipants.add(rtm.getSenderParticipantId());
            }
        }
    }

    // Broadcast my score to everybody else.
    void broadcastScore(boolean finalScore) {
        if (!mMultiplayer)
            return; // playing single-player mode
        if (mTurnBasedMatch != null)
            return; // playing in turn-based mode

        byte[] scoremsg = intToBytes(mScore);
        int len = scoremsg.length;
        byte[] mMsgBuf = new byte[len+1];

        // First byte in message indicates whether it's a final score or not
        mMsgBuf[0] = (byte) (finalScore ? 'F' : 'U');
        System.arraycopy(scoremsg, 0, mMsgBuf, 1, len);

        // Send to every other participant.
        for (Participant p : mParticipants) {
            if (p.getParticipantId().equals(mMyId))
                continue;
            if (p.getStatus() != Participant.STATUS_JOINED)
                continue;
            if (finalScore) {
                // final score notification must be sent via reliable message
                Games.RealTimeMultiplayer.sendReliableMessage(myApiClient, null, mMsgBuf,
                        mRoomId, p.getParticipantId());
            } else {
                // it's an interim score notification, so we can use unreliable
                Games.RealTimeMultiplayer.sendUnreliableMessage(myApiClient, mMsgBuf, mRoomId,
                        p.getParticipantId());
            }
        }
    }

    void keepScreenOn() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // Clears the flag that keeps the screen on.
    void stopKeepingScreenOn() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    void leaveRoom() {
        Log.d(TAG, "Leaving room.");
        mSecondsLeft = 0;
        stopKeepingScreenOn();
        if (mRoomId != null) {

            byte[] leavemsg = new byte[1];
            leavemsg[0] = (byte) 'L';

            // Send to every other participant.
            for (Participant p : mParticipants) {
                if (p.getParticipantId().equals(mMyId))
                    continue;
                if (p.getStatus() != Participant.STATUS_JOINED)
                    continue;
                // it's an interim score notification, so we can use unreliable
                Games.RealTimeMultiplayer.sendUnreliableMessage(myApiClient, leavemsg, mRoomId,
                        p.getParticipantId());
            }

            Games.RealTimeMultiplayer.leave(myApiClient, this, mRoomId);
            mRoomId = null;
            switchToMenuView();
            switchToMainScreen();
        } else {
            switchToMenuView();
            switchToMainScreen();
        }
    }

    void acceptInviteToRoom(String invId) {
        // accept the invitation
        Log.d(TAG, "Accepting invitation: " + invId);
        RoomConfig.Builder roomConfigBuilder = RoomConfig.builder(this);
        roomConfigBuilder.setInvitationIdToAccept(invId)
                .setMessageReceivedListener(this)
                .setRoomStatusUpdateListener(this);
        switchToScreen(R.id.screen_wait);
        keepScreenOn();
        resetGameVars();
        Games.RealTimeMultiplayer.join(myApiClient, roomConfigBuilder.build());
    }

    void showWaitingRoom(Room room) {
        // minimum number of players required for our game
        // For simplicity, we require everyone to join the game before we start it
        // (this is signaled by Integer.MAX_VALUE).
        final int MIN_PLAYERS = Integer.MAX_VALUE;
        Intent i = Games.RealTimeMultiplayer.getWaitingRoomIntent(myApiClient, room, MIN_PLAYERS);

        // show waiting room UI
        startActivityForResult(i, RC_WAITING_ROOM);
    }

    //Other callbacks
    @Override
    public void onRoomAutoMatching(Room room) {
        updateRoom(room);
    }

    @Override
    public void onRoomConnecting(Room room) {
        updateRoom(room);
    }

    @Override
    public void onP2PDisconnected (String string) {}

    @Override
    public void onP2PConnected(String s) {}

    @Override
    public void onPeerInvitedToRoom(Room room, List<String> strings) {
        updateRoom(room);
    }

    @Override
    public void onConnectedToRoom(Room room) {
        Log.d(TAG, "onConnectedToRoom.");

        // get room ID, participants and my ID:
        mRoomId = room.getRoomId();
        mParticipants = room.getParticipants();
        mMyId = room.getParticipantId(Games.Players.getCurrentPlayerId(myApiClient));

        // print out the list of participants (for debug purposes)
        Log.d(TAG, "Room ID: " + mRoomId);
        Log.d(TAG, "My ID " + mMyId);
        Log.d(TAG, "<< CONNECTED TO ROOM>>");
    }

    //GAME RELATED METHODS
    final static int[] SCREENS = {
            R.id.screen_main, R.id.screen_sign_in, R.id.screen_wait
    };
    int mCurScreen = -1;

    void resetGameVars() {
        mSecondsLeft = GAME_DURATION;
        mScore = 0;
        mParticipantScore.clear();
        mFinishedParticipants.clear();
    }

    public void switchToScreen(int screenId) {
        // make the requested screen visible; hide all others.
        for (int id : SCREENS) {
            findViewById(id).setVisibility(screenId == id ? View.VISIBLE : View.GONE);
        }
        mCurScreen = screenId;

        // should we show the invitation popup?
        boolean showInvPopup;
        if (mIncomingInvitationId == null) {
            // no invitation, so no popup
            showInvPopup = false;
        } else if (mMultiplayer) {
            // if in multiplayer, only show invitation on main screen
            showInvPopup = (mCurScreen == R.id.screen_main);
        } else {
            // single-player: show on main screen and gameplay screen
            showInvPopup = (mCurScreen == R.id.screen_main || mCurScreen == R.id.screen_game);
        }
        findViewById(R.id.invitation_popup).setVisibility(showInvPopup ? View.VISIBLE : View.GONE);
    }

    void switchToMainScreen() {
        if (myApiClient != null && myApiClient.isConnected()) {
            switchToScreen(R.id.screen_main);
        }
        else {
            switchToScreen(R.id.screen_sign_in);
        }
    }

    public void switchToGameView(boolean newGame) {
        if (newGame) {
            gameView = new GameView(this, mWidth, mHeight, true);
            gameView.setBoardActivity(myActivity);
        }
        setContentView(gameView);
    }

    public void switchToMenuView() {
        setContentView(R.layout.bgrams_mp_main);
    }

    private void handleInvitationInboxResult(int response, Intent data) {
        if (response != Activity.RESULT_OK) {
            Log.w(TAG, "*** invitation inbox UI cancelled, " + response);
            switchToMainScreen();
            return;
        }

        Log.d(TAG, "Invitation inbox UI succeeded.");
        Invitation inv = data.getExtras().getParcelable(Multiplayer.EXTRA_INVITATION);

        // accept invitation
        acceptInviteToRoom(inv.getInvitationId());
    }

    public byte[] intToBytes( final int i ) {
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.putInt(i);
        return bb.array();
    }

    private int bytesToInt(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getInt();
    }

    // Generic warning/info dialog
    public void showWarning(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(title).setMessage(message);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        switchToMenuView();
                        switchToMainScreen();
                    }
                });

        // create alert dialog
        AlertDialog mAlertDialog = alertDialogBuilder.create();

        // show it
        mAlertDialog.show();
    }

    public void showErrorMessage(TurnBasedMatch match, int statusCode,
                                 String string) {

        showWarning("Warning", string);
    }

    // Returns false if something went wrong, probably. This should handle
    // more cases, and probably report more accurate results.
    private boolean checkStatusCode(TurnBasedMatch match, int statusCode) {
        switch (statusCode) {
            case GamesStatusCodes.STATUS_OK:
                return true;
            case GamesStatusCodes.STATUS_NETWORK_ERROR_OPERATION_DEFERRED:
                // This is OK; the action is stored by Google Play Services and will
                // be dealt with later.
                return true;
            case GamesStatusCodes.STATUS_MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER:
                showErrorMessage(match, statusCode,"Not trusted tester");
                break;
            case GamesStatusCodes.STATUS_MATCH_ERROR_ALREADY_REMATCHED:
                showErrorMessage(match, statusCode,"Error: Already rematched");
                break;
            case GamesStatusCodes.STATUS_NETWORK_ERROR_OPERATION_FAILED:
                showErrorMessage(match, statusCode,"Network Error");
                break;
            case GamesStatusCodes.STATUS_CLIENT_RECONNECT_REQUIRED:
                showErrorMessage(match, statusCode,"Client reconnect required");
                break;
            case GamesStatusCodes.STATUS_INTERNAL_ERROR:
                showErrorMessage(match, statusCode, "Internal error");
                break;
            case GamesStatusCodes.STATUS_MATCH_ERROR_INACTIVE_MATCH:
                showErrorMessage(match, statusCode,"Error: Inactive match");
                break;
            case GamesStatusCodes.STATUS_MATCH_ERROR_LOCALLY_MODIFIED:
                showErrorMessage(match, statusCode,"Error: Locally modified");
                break;
            default:
                showErrorMessage(match, statusCode, "Unexpected error");
                Log.d(TAG, "Did not have warning or string to deal with: "
                        + statusCode);
        }
        return false;
    }

    //GAME METHODS
    private static Point getDisplaySize(final Display display) {
        final Point point = new Point();
        if (Build.VERSION.SDK_INT > 12) {
            display.getSize(point);
        } else { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        return point;
    }

    public static Context getMyContext() {
        return context;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bgrams_settings:
                startActivity(new Intent(this, Prefs.class));
                return true;
        }
        return false;
    }

    public void setMyScore(int score) {
        mScore = score;
        if (mSecondsLeft <= 0)
            return; // too late!

        // broadcast our new score to our peers
        broadcastScore(false);
    }
}
